package eventstreamhandler

import (
	"context"

	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/eventdb"
	"github.com/qobbysam/dealbase/pkgs/iface"
	"github.com/qobbysam/dealbase/pkgs/logging"
)

type EventStreamHandler struct {
	Logger     *logging.BigLogger
	EventDB    *eventdb.EventDB
	SharedCtx  context.Context
	StopChan   chan struct{}
	EventsList []iface.EventServiceInterface
}

func NewEventStreamHandler(cfg *config.BigConfig, logger *logging.BigLogger, ctx context.Context) *EventStreamHandler {

	return &EventStreamHandler{}
}
