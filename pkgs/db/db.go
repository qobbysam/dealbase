package locdb

import (
	"context"

	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/logging"
)

type DB struct {
	logger *logging.DBLogger
	Reader *DBReader
	Writer *DBWriter
	//Interpretator *Interpretator
	SharedCtx context.Context
	StopChan  chan struct{}
}

func (db_ *DB) Init() error {

	return nil
}

func (db_ *DB) StartApp(stopchan chan struct{}, errorchan chan error) {

	db_.logger.WriteInfo("Started DB Successfully")
}

// func (db_ *DB) DoAWrite(value reflect.Value) {

// 	msg, err := Interpretator.BuildMsg(value)
// 	if err != nil {
// 		fmt.Println("Interpreator could not build msg")
// 	}
// 	err = db_.Writer.DoAWrite(msg)
// 	if err != nil {
// 		fmt.Println("Interpreator could not build msg")
// 	}

// }

// func (db_ *DB) DoARead(value reflect.Value) {

// 	msg, err := Interpretator.BuildMsg(value) {
// 		if err != nil {
// 			fmt.Println("Interpreator could not build msg")
// 		}
// 		err = db_.Reader.DoARead(msg)
// 		if err != nil {
// 			fmt.Println("Interpreator could not build msg")
// 		}

// func (db_ *DB) Migrate(value interface{}) error {

// 	err := db_.Reader.Migrate(value)

// 	if err != nil {
// 		fmt.Println("Failed to migrate a value for reader  ", err)
// 		return err
// 	}

// 	err = db_.Writer.Migrate(value)

// 	if err != nil {
// 		fmt.Println("Failed to migrate a value for writer  ", err)
// 		return err
// 	}

// 	return err

// }

func NewDB(cfg *config.BigConfig, logger *logging.BigLogger, ctx context.Context) *DB {

	outdb := DB{}

	reader := NewDBReader(cfg)

	writer := NewDBWriter(cfg)

	outdb.Reader = reader
	outdb.Writer = writer
	outdb.logger = logger.DBLogger
	outdb.SharedCtx = ctx

	return &outdb
}
