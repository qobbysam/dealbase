package locdb

import "sync"

type DBReadInMsg struct{}

type DBReadOutMsg struct{}

type ReadExecuteFunc func(readmsg DBReadInMsg) DBReadOutMsg

type DBWriteInMsg struct{}

type DBWriteOutMsg struct{}

type WriteExecuteFunc func(writemsg DBWriteInMsg) DBWriteOutMsg

type Interpretator struct {
	Mu                   sync.Mutex
	ServiceReadExecutor  map[string]ReadExecuteFunc
	ServiceWriteExecutor map[string]WriteExecuteFunc
}

func BuildWriteMsg() {}

func BuildReadMsg() {

}

func RegisterFuncs() {

}

func NewInterpretator() *Interpretator {
	return &Interpretator{}
}
