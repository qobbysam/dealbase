package locdb

import (
	"fmt"

	"github.com/qobbysam/dealbase/pkgs/config"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DBReader struct {
	GormDB *gorm.DB
}

// func (dbr *DBReader) Migrate(objectpointer interface{}) error {

// 	switch objectpointer.(type) {
// 	case iface.ModelIface:
// 		value, _ := objectpointer.(iface.ModelIface)

// 		value_of_model := reflect.ValueOf(value)

// 		err := BigMigrator(dbr.GormDB, value_of_model)
// 		if err != nil {
// 			fmt.Println("Reader Migrator Failed ,  ", err)
// 			return errors.New("Failed to migrate in Reader Migrator")
// 		}
// 	default:

// 		fmt.Println("Default Called IN reader Migrate")
// 		return nil
// 		//dbr.GormDB.AutoMigrate(value_of_model.Elem().Addr())
// 	}

// 	return nil

// }

func NewDBReader(cfg *config.BigConfig) *DBReader {
	switch cfg.DBReaderConfig.IfaceType {
	case "sqlite":
		fmt.Println("cfg: ", cfg.DBReaderConfig.SqliteConfig.DBName)
		db_, err := gorm.Open(sqlite.Open(cfg.DBReaderConfig.SqliteConfig.DBName), &gorm.Config{})

		if err != nil {
			fmt.Println("Writer failed here", err)
			return nil
		}

		return &DBReader{GormDB: db_}
	case "postgres":
		msn := "host=" + cfg.DBReaderConfig.PostGresConfig.DBHost + " user=" + cfg.DBReaderConfig.PostGresConfig.DBUsername

		msn2 := msn + " password=" + cfg.DBReaderConfig.PostGresConfig.DBPassword + " dbname=" + cfg.DBReaderConfig.PostGresConfig.DBName

		msn3 := msn2 + " port=" + cfg.DBReaderConfig.PostGresConfig.DBPort + " sslmode=disable TimeZone=US/Eastern"

		db_, err := gorm.Open(postgres.Open(msn3), &gorm.Config{})

		if err != nil {
			fmt.Println("Writer Postgress here")
			return nil
		}

		return &DBReader{GormDB: db_}

	}
	return nil
}

// func BigMigrator(db *gorm.DB, value reflect.Value) error {
// 	iface_val := value.Interface()
// 	switch iface_val.(type) {

// 	case auth.UserProfile:
// 		db.AutoMigrate(&auth.UserProfile{})
// 		return nil
// 	case auth.UserGroup:
// 		db.AutoMigrate(&auth.UserGroup{})
// 		return nil
// 	case auth.UserInfo:
// 		db.AutoMigrate(&auth.UserInfo{})
// 		return nil
// 	default:
// 		fmt.Println("using default here in bigmigrator,  ", value)
// 		return nil

// 	}
// }
