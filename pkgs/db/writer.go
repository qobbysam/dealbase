package locdb

import (
	"fmt"

	//"github.com/qobbysam/dealbase/pkgs/auth"
	//	"github.com/qobbysam/dealbase/pkgs/auth"
	//	"github.com/qobbysam/dealbase/pkgs/auth"
	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/logging"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DBWriter struct {
	GormDB *gorm.DB
	Logger *logging.BigLogger
}

// func (dbw *DBWriter) Migrate(objectpointer interface{}) error {
// 	switch objectpointer.(type) {
// 	case *iface.ModelIface:
// 		value, _ := objectpointer.(*iface.ModelIface)

// 	}
// }

func (d *DBWriter) CreateDefaultGroups() error {

	// moderatorm := auth.UserGroup{
	// 	ID:    "iPwbPdD4KEznHdppmzL2TV",
	// 	Name:  "MODERATORM",
	// 	Roles: "moderatorm",
	// }

	// moderatorw := auth.UserGroup{
	// 	ID:    "PX4EeEndXPUiKqXQ59n4Lm",
	// 	Name:  "MODERATORW",
	// 	Roles: "moderatorw",
	// }

	// appsu := auth.UserGroup{
	// 	ID:    "TnCviHL72CMb6jktuHeUqd",
	// 	Name:  "APPSU",
	// 	Roles: "appsu",
	// }

	// seller := auth.UserGroup{
	// 	ID:    "jH54rbKmeuuoMfLZWb79KK",
	// 	Name:  "DEALSELLER",
	// 	Roles: "dealseller",
	// }

	// buyer := auth.UserGroup{
	// 	ID:    "dEqLm3pKhyKoznyuzVPcgE",
	// 	Name:  "DEALBUYER",
	// 	Roles: "dealbuyer",
	// }

	// grouplist_auth := []*auth.UserGroup{&moderatorm, &moderatorw, &appsu, &seller, &buyer}

	//err := d.GormDB.Create(grouplist_auth)

	// showingroom := []showing.ShowRoom{
	// 	{ID: "ROOM1", Name: "ROOM1"},
	// 	{ID: "ROOM2", Name: "ROOM2"},
	// 	{ID: "ROOM3", Name: "ROOM3"},
	// }

	// err := d.GormDB.Create(showingroom)

	// return err.Error

	return nil
}

func NewDBWriter(cfg *config.BigConfig) *DBWriter {
	switch cfg.DBWriterConfig.IfaceType {
	case "sqlite":
		db_, err := gorm.Open(sqlite.Open(cfg.DBWriterConfig.SqliteConfig.DBName), &gorm.Config{})

		if err != nil {
			fmt.Println("Writer failed here")
			return nil
		}

		return &DBWriter{GormDB: db_}
	case "postgres":
		msn := "host=" + cfg.DBWriterConfig.PostGresConfig.DBHost + " user=" + cfg.DBWriterConfig.PostGresConfig.DBUsername

		msn2 := msn + " password=" + cfg.DBWriterConfig.PostGresConfig.DBPassword + " dbname=" + cfg.DBWriterConfig.PostGresConfig.DBName

		msn3 := msn2 + " port=" + cfg.DBWriterConfig.PostGresConfig.DBPort + " sslmode=disable TimeZone=US/Eastern"

		db_, err := gorm.Open(postgres.Open(msn3), &gorm.Config{})

		if err != nil {
			fmt.Println("Writer Postgress here")
			return nil
		}

		return &DBWriter{GormDB: db_}

	}
	return nil
}
