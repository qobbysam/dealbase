package restservicehandler

import (
	"context"

	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/logging"
)

type RestServiceHandler struct{}

func NewRestServiceHandler(cfg *config.BigConfig, logger *logging.BigLogger, ctx context.Context) *RestServiceHandler {

	return &RestServiceHandler{}
}
