package auth

import (
	"net/http"

	"github.com/go-chi/chi"
)

func (acs *AuthClientServer) RegisterRoutes(rout chi.Router) error {

	// fmt.Println("authclien register called")
	// group := acs.R.Group(func(r chi.Router) {
	// 	r.Post("/signup", acs.SignUpHandler)
	// 	r.Get("/get-info", acs.GetInfoHandeler)
	// 	r.Get("/get-all-group", acs.GetAllGroupHandler)
	// 	r.Post("/add-group", acs.AddGroupHandler)
	// 	r.Get("/get-one-group", acs.GetOneGroupHandler)
	// })

	// rout.Mount("/acs", group)
	return nil
}

// func (acs *AuthClientServer) Handler(w http.ResponseWriter, r *http.Request) {

// }

func (acs *AuthClientServer) GetInfoHandeler(w http.ResponseWriter, r *http.Request) {
	acs.GetInfo(w, r)
}

func (acs *AuthClientServer) SignUpHandler(w http.ResponseWriter, r *http.Request) {
	acs.SignUp(w, r)
}

func (acs *AuthClientServer) GetAllGroupHandler(w http.ResponseWriter, r *http.Request) {

	acs.GetAllGroup(w, r)

}

func (acs *AuthClientServer) AddGroupHandler(w http.ResponseWriter, r *http.Request) {
	acs.AddGroup(w, r)
}

func (acs *AuthClientServer) GetOneGroupHandler(w http.ResponseWriter, r *http.Request) {

	acs.GetOneGroup(w, r)
}
