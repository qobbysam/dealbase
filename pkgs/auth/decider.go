package auth

import "github.com/qobbysam/dealbase/pkgs/config"

type Decider struct {
	SUSUKey             string
	ModeratorManagerKey string
	ModeratorWorkerKey  string
	DealSellerKey       string
	DealBuyerKey        string
	AppSUKey            string
}

func NewDecider(cfg *config.BigConfig) *Decider {

	return &Decider{
		SUSUKey:             cfg.AuthServerConfig.RoutesConfig.SuSuKey,
		ModeratorManagerKey: cfg.AuthServerConfig.RoutesConfig.ModeratorManagerKey,
		ModeratorWorkerKey:  cfg.AuthServerConfig.RoutesConfig.ModeratorWorkerKey,
		DealSellerKey:       cfg.AuthServerConfig.RoutesConfig.DealSellerKey,
		DealBuyerKey:        cfg.AuthServerConfig.RoutesConfig.DealBuyerKey,
		AppSUKey:            cfg.AuthServerConfig.RoutesConfig.AppSuKey,
	}
}
