package auth

import (
	"net/http"

	"github.com/go-chi/render"
)

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

type UserResponse struct {
	//*UserProfile
	// HTTPStatusCode int
	// StatusText     string
	//Elapsed int64 `json:"elapsed"`

	//Username string
	//Active string
	//roles, name, avatar, introduction
	Roles        []string `json:"roles"`
	Name         string   `json:"name"`
	Avatar       string   `json:"avatar"`
	Introduction string   `json:"introduction"`
}

// func (rr *UserResponse) Render(w http.ResponseWriter, r *http.Request) error {
// 	render.Status(r, rr.HTTPStatusCode)

// 	return nil
// 	// return w.Write((rr.Data).toJson)
// }

type AllGroupResponse struct {
	Data []UserGroup `json:"data"`
}

func (al *AllGroupResponse) Render(w http.ResponseWriter, r *http.Request) error {

	return nil

}

type OneGroupResponse struct {
	Data UserGroup `json:"data"`
}

func (al *OneGroupResponse) Render(w http.ResponseWriter, r *http.Request) error {

	return nil

}
func NewOneGroupResponse(group UserGroup) *OneGroupResponse {

	return &OneGroupResponse{Data: group}
}

func NewAllGroupResponse(usergroup []UserGroup) *AllGroupResponse {
	return &AllGroupResponse{Data: usergroup}
}
func NewUserResponse(user UserProfile) *UserResponse {
	return &UserResponse{

		Roles:        []string{user.UserGroup.Roles},
		Name:         user.UserInfo.Username,
		Avatar:       "https://via.placeholder.com/150",
		Introduction: user.UserInfo.Username,
	}
}

func (rd *UserResponse) Render(w http.ResponseWriter, r *http.Request) error {
	// Pre-processing before a response is marshalled and sent across the wire

	//rd.Elapsed = 10
	return nil
}
