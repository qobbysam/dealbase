package auth

import (
	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
)

type AuthRestServer struct {
	DB            *db.DB
	R             chi.Router
	Client        *AuthClientServer
	WriteRegister map[string]db.WriteExecuteFunc
	ReadRegister  map[string]db.ReadExecuteFunc
}

func (as *AuthRestServer) BuildRoutes(chin chi.Router) chi.Router {

	loc_router := chi.NewRouter()

	clientroutes := as.Client.BuildRoutes(loc_router)

	chin.Mount("/account", clientroutes)

	return chin
}

func NewAuthServer(cfg *config.BigConfig, dbin *db.DB) (*AuthRestServer, error) {

	out := AuthRestServer{}

	client, err := NewAuthClient(cfg, dbin)

	//router.Handle("/client", client.R)

	if err != nil {
		return nil, err
	}
	//router := out.Routes()
	//out.R = router
	out.Client = client
	out.DB = dbin

	return &out, nil
}

// func (as *AuthRestServer) RegisterRoutes(rout chi.Router) error {

// 	err := as.Client.RegisterRoutes(rout)

// 	if err != nil {
// 		return err
// 	}

// 	rout.Handle("/client", as.R)
// 	//as.R.Handle("/client", as.Client.R)

// 	return nil
// }
