package auth

import (
	"errors"
	"fmt"
	"log"

	"github.com/lithammer/shortuuid"
)

func (db *DBEX) CreateUser(group_ID string, uid string, username string) (*UserProfile, error) {

	id := shortuuid.New()

	group, err := db.GetOneGroup(group_ID)
	if err != nil {
		msg := fmt.Sprint("Error in group id , ", err)
		db.DB.Writer.Logger.WriteInfo(msg)
	}
	info := UserInfo{
		ID:       id,
		Username: username,
		//UserProfileID: id,
	}

	infoobj, err := db.CreateInfo(info)
	if err != nil {

		return nil, err
	}

	out := UserProfile{}

	out.ID = id
	out.UID = uid
	//out.UserInfo = *infoobj
	out.UserGroupID = group_ID
	out.UserGroup = *group
	out.UserInfoID = id
	out.UserInfo = *infoobj

	//user := UserProfile{ID: id, UID: uid, UserInfo: info, UserGroup: *group, UserGroupID: group.ID}

	result := db.DB.Writer.GormDB.Create(&out)

	log.Println(result.RowsAffected)
	log.Println(result.Error)

	if result.Error != nil {
		return nil, result.Error
	}
	return &out, nil
}

func (db *DBEX) GetUser(userid string) (*UserProfile, error) {

	var allusers []UserProfile
	user := UserProfile{}
	// Struct

	db.DB.Reader.GormDB.Find(&allusers)
	db.DB.Reader.GormDB.Where(&UserProfile{UID: userid}).First(&user)

	info, err := db.GetOneInfo(user.UserInfoID)

	if err != nil {
		return nil, err
	}

	group, err := db.GetOneGroup(user.UserGroupID)
	if err != nil {
		return nil, err
	}
	user.UserInfo = *info
	user.UserGroup = *group
	//db.DB.First(&user, "uid = ?", userid)
	log.Println("all users here ", allusers)

	log.Println("found user here ", user)
	if user.ID == "" {
		return &user, errors.New("user not found")
	}
	return &user, nil

}

func (db *DBEX) CreateGroup(name, permissions string) (*UserGroup, error) {

	id := shortuuid.New()

	usergroup := UserGroup{}
	usergroup.ID = id
	usergroup.Roles = permissions
	usergroup.Name = name

	result := db.DB.Reader.GormDB.Create(&usergroup)

	if result.Error != nil {

		return nil, result.Error
	}
	log.Println(result.RowsAffected)
	log.Println(result.Error)

	return &usergroup, nil
}

func (db *DBEX) GetAllGroup() ([]UserGroup, error) {

	var usergroup []UserGroup

	result := db.DB.Writer.GormDB.Find(&usergroup)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		db.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	return usergroup, nil

}

func (db *DBEX) GetOneGroup(userid string) (*UserGroup, error) {
	var usergroup UserGroup

	result := db.DB.Reader.GormDB.Where(&UserGroup{ID: userid}).First(&usergroup)

	if result.Error != nil {
		return nil, result.Error
	}

	return &usergroup, nil

}

func (db *DBEX) CreateInfo(info UserInfo) (*UserInfo, error) {

	fmt.Println("info in create info  ", info)
	out := UserInfo{
		ID:       info.ID,
		Username: info.Username,
		//	UserProfileID: info.UserProfileID,
	}
	result := db.DB.Writer.GormDB.Create(&out)

	if result.Error != nil {
		return nil, result.Error
	}

	fmt.Println("out info here ", out)
	return &out, nil
}

func (db *DBEX) GetOneInfo(infoid string) (*UserInfo, error) {

	info := UserInfo{}
	result := db.DB.Reader.GormDB.Where(&UserInfo{ID: infoid}).First(&info)

	if result.Error != nil {
		return nil, result.Error
	}

	return &info, nil

}
