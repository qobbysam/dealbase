package auth

import (
	"errors"
	"net/http"
)

type SignupRequest struct {
	Token    string `json:"token"`
	Username string `json:"username"`
	Groupid  string `json:"groupid"`
	//Roles    []string `json:"roles"`
}

type AddGroupRequest struct {
	Name        string `json:"name"`
	Permissions string `json:"permissions"`
}

func (a *AddGroupRequest) Bind(r *http.Request) error {
	// a.Article is nil if no Article fields are sent in the request. Return an
	// error to avoid a nil pointer dereference.
	// if a.Article == nil {
	// 	return errors.New("missing required Article fields.")
	// }

	if a.Name == "" {
		return errors.New("Name Field")
	}

	if a.Permissions == "" {
		return errors.New("Missing permissions field")
	}
	// a.User is nil if no Userpayload fields are sent in the request. In this app
	// this won't cause a panic, but checks in this Bind method may be required if
	// a.User or futher nested fields like a.User.Name are accessed elsewhere.

	// just a post-process after a decode..
	// a.ProtectedID = ""                                 // unset the protected ID
	// a.Article.Title = strings.ToLower(a.Article.Title) // as an example, we down-case
	return nil
}

func (a *SignupRequest) Bind(r *http.Request) error {
	// a.Article is nil if no Article fields are sent in the request. Return an
	// error to avoid a nil pointer dereference.
	// if a.Article == nil {
	// 	return errors.New("missing required Article fields.")
	// }

	if a.Token == "" {
		return errors.New("Missing Token Field")
	}

	if a.Groupid == "" {
		return errors.New("Missing group id")
		//a.Roles = append(a.Roles, "test")
	}

	if a.Username == "" {
		return errors.New("Missing username field")
	}
	// a.User is nil if no Userpayload fields are sent in the request. In this app
	// this won't cause a panic, but checks in this Bind method may be required if
	// a.User or futher nested fields like a.User.Name are accessed elsewhere.

	// just a post-process after a decode..
	// a.ProtectedID = ""                                 // unset the protected ID
	// a.Article.Title = strings.ToLower(a.Article.Title) // as an example, we down-case
	return nil
}
