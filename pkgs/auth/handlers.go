package auth

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/render"
)

func (acs *AuthClientServer) GetInfo(w http.ResponseWriter, r *http.Request) {

	//csrf
	//csrf, err := r.Header.Get("")

	// for k, v := range r.Header {
	// 	fmt.Fprintf(w, "Header field %q, Value %q\n", k, v)
	// }

	// //token := chi.URLParam(r, "pc_token")
	// tokenCookie, err := r.Cookie("token")
	// if err != nil {
	// 	log.Fatalf("Error occured while reading cookie")
	// }
	// fmt.Println("\nPrinting cookie with name as token")
	// fmt.Println(tokenCookie)

	// fmt.Println("\nPrinting all cookies")
	// for _, c := range r.Cookies() {

	// 	fmt.Println(c)
	// }

	token := r.URL.Query().Get("token")

	if token != "" {

		sub, err := acs.ClientAuth.SubjectFrom(token)

		if err != nil {
			//break process and return error sign up
			render.Render(w, r, ErrInvalidRequest(err))
			return
		}

		fmt.Println("sub found here ", sub)
		user, err := acs.DBEX.GetUser(sub)

		if err != nil {
			//break process and return error sign up
			render.Render(w, r, ErrInvalidRequest(err))
			return
		}

		render.Status(r, http.StatusOK)
		render.Render(w, r, NewAntDRoutes(user, token, acs.Decider))
		return

	}
	log.Println("Printing token ", token)
	render.Render(w, r, ErrInvalidRequest(errors.New("token  not valid")))
}

func (acs *AuthClientServer) SignUp(w http.ResponseWriter, r *http.Request) {

	data := &SignupRequest{}

	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	log.Println(data)
	sub, err := acs.ClientAuth.SubjectFrom(data.Token)

	if err != nil {
		//break process and return error sign up
		fmt.Println("Subfining failed ", err)
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	if sub == "" {
		fmt.Println("Subfining failed ", err)
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	//roles_one := strings.Join(data.Roles[:], ",")

	user, err := acs.DBEX.CreateUser(data.Groupid, sub, data.Username)

	log.Println("userCreated  ppp   ppp   ", user)

	render.Status(r, http.StatusCreated)
	render.Render(w, r, NewUserResponse(*user))

	// fmt.Println(sub)
	// fmt.Println(err)

	// url := "https://auth.pcarriers.com/accounts"
	// var username string
	// var pasword string

	// data := url.Values{
	// 	"username": {username},
	// 	"password": {password},
	// }

	// req,_ := http.NewRequest("POST", accountsURL.String(), strings.NewReader(data.Encode()))

	// req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	// req.Header.Set("Origin", "https://app.pcarriers.com")

	// fmt.Println(req.)
}

func (acs *AuthClientServer) GetAllGroup(w http.ResponseWriter, r *http.Request) {

	groups, err := acs.DBEX.GetAllGroup()

	if err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	render.Render(w, r, NewAllGroupResponse(groups))
	return
}

func (acs *AuthClientServer) GetOneGroup(w http.ResponseWriter, r *http.Request) {

	id := r.URL.Query().Get("groupid")

	if id == "" {
		render.Render(w, r, ErrInvalidRequest(errors.New("group id not in params")))
		return
	}

	group, err := acs.DBEX.GetOneGroup(id)

	if err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	render.Render(w, r, NewOneGroupResponse(*group))
	return
}

func (acs *AuthClientServer) AddGroup(w http.ResponseWriter, r *http.Request) {

	data := &AddGroupRequest{}

	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	log.Println(data)

	group, err := acs.DBEX.CreateGroup(data.Name, data.Permissions)

	if err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	render.Render(w, r, NewOneGroupResponse(*group))
	return
}
