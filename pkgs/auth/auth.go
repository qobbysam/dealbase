package auth

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/iface"
	"github.com/qobbysam/dealbase/pkgs/logging"
)

type AuthEventServer struct{}

type AuthObject struct {
	DB         *db.DB
	DBEX       *DBEX
	AuthModels []iface.ModelIface
	LoggerRest *logging.RestLogger
	SharedCtx  context.Context

	StopChan        chan struct{}
	AuthRestServer  *AuthRestServer
	AuthEventServer *AuthEventServer
}

func (ao *AuthObject) RegisterSuRoutes() {}

func (ao *AuthObject) RegisterAppRoutes() {

}

func (ao *AuthObject) BuildMigrations() error {
	// all_migratrions := []interface{}{
	// 	UserProfile{},
	// 	UserInfo{},
	// 	UserGroup{},
	// }

	err := ao.DB.Writer.GormDB.AutoMigrate(UserProfile{}, UserInfo{}, UserGroup{})
	//	return all_migratrions

	//_ = ao.DBEX.CreateDefaultGroups()
	return err
}

func (ao *AuthObject) RegisterRestService() (chi.Router, string) {
	authpattern := "/auth"
	router_builder := chi.NewRouter()
	out_router := chi.NewRouter()

	authserverroutes := ao.AuthRestServer.BuildRoutes(router_builder)
	// err := ao.AuthRestServer.RegisterRoutes(router)

	//r.Mount("/", authserverroutes)

	// if err != nil {
	// 	fmt.Println("error registering rouers , ", err)
	// }
	//router.("/account", ao.AuthRestServer.R)

	out_router.Mount("/", authserverroutes)

	return out_router, authpattern
}

func (ao *AuthObject) CreateModels() error {
	userprofile := UserProfile{}
	userinfo := UserInfo{}
	usergroup := UserGroup{}

	ao.RegisterModel(&userinfo)
	ao.RegisterModel(&usergroup)
	ao.RegisterModel(&userprofile)
	return nil
}

func (ao *AuthObject) RegisterModel(ifacemodel iface.ModelIface) error {

	ao.AuthModels = append(ao.AuthModels, ifacemodel)
	return nil
}

func (ao *AuthObject) RegisterModels(ifacemodels []iface.ModelIface) error {

	ao.AuthModels = append(ao.AuthModels, ifacemodels...)
	return nil
}

func (ao *AuthObject) DoMigrations() error {
	//reader Migrations
	for _, v := range ao.AuthModels {
		err := v.DoMigrations(ao.DB)
		if err != nil {
			fmt.Println("failed to migrate this  model with this error , ", err)

			return errors.New("failed to do migration in authobject domigration")
		}
	}
	//writer Migrations

	return nil
}

// func (ao *AuthObject) Init() error {

// 	err := ao.AuthRestServer.RegisterRoutes()
// 	return err
// }

func NewAuthObject(cfg *config.BigConfig, dbconn *db.DB, logger *logging.RestLogger) *AuthObject {
	outobject := AuthObject{}

	autrestserver, err := NewAuthServer(cfg, dbconn)

	if err != nil {
		fmt.Println("object:   ", err)
	}

	outobject.AuthRestServer = autrestserver
	outobject.DB = dbconn
	outobject.LoggerRest = logger
	outobject.DBEX = &DBEX{DB: dbconn}

	err = outobject.BuildMigrations()

	if err != nil {
		msg := fmt.Sprint("error in doing migrations : ", err)
		logger.WriteInfo(msg)
	}
	logger.WriteInfo("Done Migrations ")
	return &outobject

}
