package auth

import (
	"fmt"
	"net/http"
)

type Children []RouteChild

// type (c Children) ToJson() error {

// }

type SeniorRoute struct {
	Router string `json:"router"`
	//Child    []string
	//Child []string
	Children []RouteChild `json:"children"`
}

type RouteChild struct {
	Router   string   `json:"router,omitempty"`
	Children []string `json:"children,omitempty"`
	//Parent    *RouteChildren
	Name      string `json:"name,omitempty"`
	Icon      string `json:"icon,omitempty"`
	Link      string `json:"link,omitempty"`
	Authority string `json:"authority,omitempty"`
	Path      string `json:"path,omitempty"`
}

//   type OutData struct {
// 	  Route string
// 	  Children []RouteChildren
//   }

type AntDUser struct {
	Name     string `json:"name,omitempty"`
	Avatar   string `json:"avatar,omitempty"`
	Address  string `json:"address,omitempty"`
	Position string `json:"position,omitempty"`
}
type AntData struct {
	User        AntDUser      `json:"user,omitempty"`
	Token       string        `json:"token,omitempty"`
	ExpireAt    string        `json:"expireAt,omitempty"`
	Roles       []string      `json:"roles"`
	Permissions []string      `json:"permissions,omitempty"`
	Data        []SeniorRoute `json:"data,omitempty"`
}
type AntDRoutes struct {
	//User string

	Code int     `json:"code"`
	Data AntData `json:"data"`
}

type rootroutesforsu struct {
	Name string
}

// var SuRoutes = RouteChildren{
// 	Name:     "Root",
// 	Children: []RouteChildren{},
// }
var suchildren = []RouteChild{
	{Router: "su", Children: []string{"fixedCreate", "userCreate", "allLocation", "newRecommendations"}},

	{Router: "Discovery", Children: []string{"allDeals", "producerEndpoint", "editUpdate"}},
	{Router: "Staging", Children: []string{"allStaged", "adminShowingRoom"}},
	{Router: "Bidding", Children: []string{"adminLiveRoom"}},
	{Router: "Inclosing", Children: []string{"allInclosing"}},
	{Router: "Accounting", Children: []string{"allAccounting"}},
	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
	{Router: "Seller", Children: []string{"sellerActive", "sellerProducer"}},
	{Router: "Buyer", Children: []string{"buyerActive", "buyerShowingRoom", "buyerLiveRoom"}},
}

var moderatormanagerchildren = []RouteChild{
	//	{Router: "moderator", Children: []string{"moderatorActive"}},
	{Router: "Discovery", Children: []string{"allDeals", "producerEndpoint", "editUpdate"}},
	{Router: "Staging", Children: []string{"allStaged", "adminShowingRoom"}},
	{Router: "Bidding", Children: []string{"adminLiveRoom"}},
	{Router: "Inclosing", Children: []string{"allInclosing"}},
	{Router: "Accounting", Children: []string{"allAccounting"}},
	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
}

var moderatorworkerchildren = []RouteChild{
	{Router: "Staging", Children: []string{"allStaged", "adminShowingRoom"}},
	{Router: "Bidding", Children: []string{"adminLiveRoom"}},
	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
}

var dealsellerchildren = []RouteChild{
	{Router: "Seller", Children: []string{"sellerActive", "sellerProducer"}},

	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
}

var dealbuyerchildren = []RouteChild{
	{Router: "Buyer", Children: []string{"buyerActive", "buyerShowingRoom", "buyerLiveRoom"}},
	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
}

var appsuchildren = []RouteChild{
	{Router: "su", Children: []string{"fixedCreate", "userCreate", "allLocation", "newRecommendations"}},

	{Router: "Discovery", Children: []string{"allDeals", "producerEndpoint", "editUpdate"}},
	{Router: "Staging", Children: []string{"allStaged", "adminShowingRoom"}},
	{Router: "Bidding", Children: []string{"adminLiveRoom"}},
	{Router: "Inclosing", Children: []string{"allInclosing"}},
	{Router: "Accounting", Children: []string{"allAccounting"}},
	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
	{Router: "Seller", Children: []string{"sellerActive", "sellerProducer"}},
	{Router: "Buyer", Children: []string{"buyerActive", "buyerShowingRoom", "buyerLiveRoom"}},
}

var unkwownchildren = []RouteChild{
	{Router: "dashboard", Children: []string{"workplace", "analysis"}},
}

var SuRoutes = SeniorRoute{
	Router:   "root",
	Children: suchildren,
}

var user = AntDUser{
	Name: "test Name",
}

func NewAntDRoutes(user *UserProfile, token string, decider *Decider) *AntDRoutes {

	//outroute := []SeniorRoute{}

	outRoute := SeniorRoute{
		Router: "root",
	}

	user_info := user.UserInfo

	fmt.Println("user  ", user)

	switch user.UserGroupID {
	case decider.SUSUKey:
		outRoute.Children = suchildren
	case decider.ModeratorManagerKey:
		outRoute.Children = moderatormanagerchildren
	case decider.ModeratorWorkerKey:
		outRoute.Children = moderatorworkerchildren
	case decider.DealBuyerKey:
		outRoute.Children = dealbuyerchildren
	case decider.DealSellerKey:
		outRoute.Children = dealsellerchildren
	case decider.AppSUKey:
		outRoute.Children = appsuchildren
	default:
		outRoute.Children = unkwownchildren
		//	outroute = append(outroute, carrierdriverRoutes)
	}

	return &AntDRoutes{
		Code: 0,
		Data: AntData{
			User:        AntDUser{Name: user_info.Username},
			Token:       token,
			Roles:       []string{user.UserGroup.Name},
			ExpireAt:    "",
			Permissions: []string{user.UserGroup.Roles},
			Data:        []SeniorRoute{outRoute},
		},
	}
}
func (rd *AntDRoutes) Render(w http.ResponseWriter, r *http.Request) error {
	// Pre-processing before a response is marshalled and sent across the wire

	//rd.Elapsed = 10
	return nil
}

// var brodismanagerchildren = []RouteChild{
// 	//{Router: "root"},
// 	//{Router: "", Children: []string{"dashboard", "workplace"}},

// 	{Router: "brodishManager", Children: []string{"bdmDash", "bdmCarrierDispatchCurrentDay", "bdmWorkerDispatchCurrentDay",
// 		"bdmCarrierDispatchPast", "bdmWorkerDispatchPast", "bdmCarrierUpcoming", "bdmWorkerUpcoming", "bdmCarrierCompliance", "bdmWorkerCompliance",
// 		"bdmRecommendation", "bdmCommunication", "bdmNotification"}},
// 	// {Router: "brodishWorker", Children: []string{"bdwDash", "bdwCarrierDispatchCurrentDay", "bdwCarrierDispatchUpcoming",
// 	// 	"bdwCarrierDispatchPast", "bdwCarrierCompliance", "bdwCarrierInformation", "bdwRecommendation", "bdwCommunication",
// 	// 	"bdwNotifications"}},
// 	// {Router: "carrierOwner", Children: []string{"crrOwnerDash", "crrOwnerCurrentDay", "crrOwnerUpcoming",
// 	// 	"crrOwnerPast", "crrOwnerRecommendations", "crrOwnerCommunication", "crrOwnerCompliance", "crrOwnerNotifications"}},
// 	// {Router: "carrierDriver", Children: []string{"crrDriverDash", "crrDriverCurrentDay", "crrDriverUpcoming",
// 	// 	"crrDriverPast", "crrDriverCommunication", "crrDriverNotifications"}},
// 	{Router: "dashboard", Children: []string{"workplace", "analysis"}},

// 	//	{Router: "form", Children: []string{"basicForm", "stepForm", "advanceForm"}},
// 	//	{Router: "basicForm", Name: "basicform", Icon: "ant-design", Authority: "queryForm"},
// 	//{Router: "antdv", Path: "antdv", Name: "ant vue design", Icon: "ant-design", Link: "https://google.com"},
// 	//{Router: "su", Children: []string{"dashboad"}},
// }

// var brodishManagerRoutes = SeniorRoute{
// 	Router:   "root",
// 	Children: brodismanagerchildren,
// }

// var brodishworkerchildern = []RouteChild{
// 	//{Router: "root"},
// 	// //{Router: "", Children: []string{"dashboard", "workplace"}},
// 	// {Router: "su", Children: []string{"suDash", "fixedCreate", "userCreate", "allLocation", "newRecommendations"}},

// 	// {Router: "brodishManager", Children: []string{"bdmDash", "bdmCarrierDispatchCurrentDay", "bdmWorkerDispatchCurrentDay",
// 	// 	"bdmCarrierDispatchPast", "bdmWorkerDispatchPast", "bdmCarrierUpcoming", "bdmWorkerUpcoming", "bdmCarrierCompliance", "bdmWorkerCompliance",
// 	// 	"bdmRecommendation", "bdmCommunication", "bdmNotification"}},
// 	{Router: "brodishWorker", Children: []string{"bdwDash", "bdwCarrierDispatchCurrentDay", "bdwCarrierDispatchUpcoming",
// 		"bdwCarrierDispatchPast", "bdwCarrierCompliance", "bdwCarrierInformation", "bdwRecommendation", "bdwCommunication",
// 		"bdwNotifications"}},
// 	// {Router: "carrierOwner", Children: []string{"crrOwnerDash", "crrOwnerCurrentDay", "crrOwnerUpcoming",
// 	// 	"crrOwnerPast", "crrOwnerRecommendations", "crrOwnerCommunication", "crrOwnerCompliance", "crrOwnerNotifications"}},
// 	// {Router: "carrierDriver", Children: []string{"crrDriverDash", "crrDriverCurrentDay", "crrDriverUpcoming",
// 	// 	"crrDriverPast", "crrDriverCommunication", "crrDriverNotifications"}},
// 	{Router: "dashboard", Children: []string{"workplace", "analysis"}},

// 	//	{Router: "form", Children: []string{"basicForm", "stepForm", "advanceForm"}},
// 	//	{Router: "basicForm", Name: "basicform", Icon: "ant-design", Authority: "queryForm"},
// 	//{Router: "antdv", Path: "antdv", Name: "ant vue design", Icon: "ant-design", Link: "https://google.com"},
// 	//{Router: "su", Children: []string{"dashboad"}},
// }

// var brodishworkerRoutes = SeniorRoute{
// 	Router:   "root",
// 	Children: brodishworkerchildern,
// }

// var carrierownerchildren = []RouteChild{
// 	//{Router: "root"},
// 	// //{Router: "", Children: []string{"dashboard", "workplace"}},
// 	// {Router: "su", Children: []string{"suDash", "fixedCreate", "userCreate", "allLocation", "newRecommendations"}},

// 	// {Router: "brodishManager", Children: []string{"bdmDash", "bdmCarrierDispatchCurrentDay", "bdmWorkerDispatchCurrentDay",
// 	// 	"bdmCarrierDispatchPast", "bdmWorkerDispatchPast", "bdmCarrierUpcoming", "bdmWorkerUpcoming", "bdmCarrierCompliance", "bdmWorkerCompliance",
// 	// 	"bdmRecommendation", "bdmCommunication", "bdmNotification"}},
// 	// {Router: "brodishWorker", Children: []string{"bdwDash", "bdwCarrierDispatchCurrentDay", "bdwCarrierDispatchUpcoming",
// 	// 	"bdwCarrierDispatchPast", "bdwCarrierCompliance", "bdwCarrierInformation", "bdwRecommendation", "bdwCommunication",
// 	// 	"bdwNotifications"}},
// 	{Router: "carrierOwner", Children: []string{"crrOwnerDash", "crrOwnerCurrentDay", "crrOwnerUpcoming",
// 		"crrOwnerPast", "crrOwnerRecommendations", "crrOwnerCommunication", "crrOwnerCompliance", "crrOwnerNotifications"}},
// 	// {Router: "carrierDriver", Children: []string{"crrDriverDash", "crrDriverCurrentDay", "crrDriverUpcoming",
// 	// 	"crrDriverPast", "crrDriverCommunication", "crrDriverNotifications"}},
// 	{Router: "dashboard", Children: []string{"workplace", "analysis"}},

// 	//	{Router: "form", Children: []string{"basicForm", "stepForm", "advanceForm"}},
// 	//	{Router: "basicForm", Name: "basicform", Icon: "ant-design", Authority: "queryForm"},
// 	//{Router: "antdv", Path: "antdv", Name: "ant vue design", Icon: "ant-design", Link: "https://google.com"},
// 	//{Router: "su", Children: []string{"dashboad"}},
// }

// var carrierownerRoutes = SeniorRoute{
// 	Router:   "root",
// 	Children: carrierownerchildren,
// }

// var carrierdriverchildren = []RouteChild{
// 	//{Router: "root"},
// 	//{Router: "", Children: []string{"dashboard", "workplace"}},
// 	// {Router: "su", Children: []string{"suDash", "fixedCreate", "userCreate", "allLocation", "newRecommendations"}},

// 	// {Router: "brodishManager", Children: []string{"bdmDash", "bdmCarrierDispatchCurrentDay", "bdmWorkerDispatchCurrentDay",
// 	// 	"bdmCarrierDispatchPast", "bdmWorkerDispatchPast", "bdmCarrierUpcoming", "bdmWorkerUpcoming", "bdmCarrierCompliance", "bdmWorkerCompliance",
// 	// 	"bdmRecommendation", "bdmCommunication", "bdmNotification"}},
// 	// {Router: "brodishWorker", Children: []string{"bdwDash", "bdwCarrierDispatchCurrentDay", "bdwCarrierDispatchUpcoming",
// 	// 	"bdwCarrierDispatchPast", "bdwCarrierCompliance", "bdwCarrierInformation", "bdwRecommendation", "bdwCommunication",
// 	// 	"bdwNotifications"}},
// 	// {Router: "carrierOwner", Children: []string{"crrOwnerDash", "crrOwnerCurrentDay", "crrOwnerUpcoming",
// 	// 	"crrOwnerPast", "crrOwnerRecommendations", "crrOwnerCommunication", "crrOwnerCompliance", "crrOwnerNotifications"}},
// 	{Router: "carrierDriver", Children: []string{"crrDriverDash", "crrDriverCurrentDay", "crrDriverUpcoming",
// 		"crrDriverPast", "crrDriverCommunication", "crrDriverNotifications"}},
// 	{Router: "dashboard", Children: []string{"workplace", "analysis"}},

// 	//	{Router: "form", Children: []string{"basicForm", "stepForm", "advanceForm"}},
// 	//	{Router: "basicForm", Name: "basicform", Icon: "ant-design", Authority: "queryForm"},
// 	//{Router: "antdv", Path: "antdv", Name: "ant vue design", Icon: "ant-design", Link: "https://google.com"},
// 	//{Router: "su", Children: []string{"dashboad"}},
// }

// var carrierdriverRoutes = SeniorRoute{
// 	Router:   "root",
// 	Children: carrierdriverchildren,
// }
