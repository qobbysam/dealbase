package auth

import db "github.com/qobbysam/dealbase/pkgs/db"

type UserProfile struct {
	ID          string `gorm:"primaryKey"`
	UID         string `json:"uid"`
	UserInfoID  string
	UserInfo    UserInfo `json:"info"`
	UserGroupID string
	UserGroup   UserGroup `json:"group"`
}

func (up *UserProfile) DoMigrations(dbin *db.DB) error {
	dbin.Reader.GormDB.AutoMigrate(&UserProfile{})
	dbin.Writer.GormDB.AutoMigrate(&UserProfile{})
	//dbobj.Reader
	return nil
}

type UserInfo struct {
	ID       string `gorm:"primaryKey" ,json:"key"`
	Username string `json:"username"`
	//UserProfileID string
}

func (ui *UserInfo) DoMigrations(dbin *db.DB) error {
	dbin.Reader.GormDB.AutoMigrate(&UserInfo{})
	dbin.Writer.GormDB.AutoMigrate(&UserInfo{})

	return nil
}

// func (ui *UserInfo) Scan(b interface{}) error {

// 	return nil
// }

type UserGroup struct {
	ID    string `gorm:"primaryKey" ,json:"key"`
	Name  string `json:"name"`
	Roles string `json:"roles"`
	//UserProfileID string
}

func (ug *UserGroup) DoMigrations(dbin *db.DB) error {
	dbin.Reader.GormDB.AutoMigrate(&UserGroup{})
	dbin.Writer.GormDB.AutoMigrate(&UserGroup{})
	return nil
}
