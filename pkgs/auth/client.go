package auth

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/keratin/authn-go/authn"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
)

type AuthConnector struct {
	VerifyUrl string
}

type ConnectorResponse struct {
	Token string `json:"token"`
	Pid   string `json:"pid"`
}

func (ac *AuthConnector) SubjectFrom(token string) (string, error) {
	added := "token"
	//path := ac.VerifyUrl + added
	log.Println("Starting Subjectfrom connector")
	log.Println("path: ", ac.VerifyUrl)
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodGet, ac.VerifyUrl, nil)
	if err != nil {
		log.Fatal(err)
	}

	// appending to existing query args
	q := req.URL.Query()
	q.Add(added, token)

	// assign encoded query string to http request
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	// if err != nil {
	// 	fmt.Println("Errored when sending request to the server")
	// 	return ""
	// }

	// log.Println("Starting Subjectfrom connector")
	// resp, err := http.Get(path)

	if err != nil {
		//	log.Fatalln(err)
		log.Println("error ", err)
		return "", errors.New("connector request failed token")
	}
	//We Read the response body on the line below.
	res_int := resp.StatusCode

	// if err != nil {
	// 	log.Println("failed to marshal response body", err)
	// 	return "", errors.New("badd responses")
	// }

	switch res_int {

	case http.StatusOK:
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("failed to read response body")
			//log.Fatalln(err)
			return "", errors.New("badd responses body")
		}

		var res ConnectorResponse

		err = json.Unmarshal(body, &res)

		if err != nil {
			log.Println("failed to marshal response body", err)

			//log.Fatalln(err)
			return "", errors.New("badd response marshalling")
		}
		return res.Pid, nil

	default:
		return "", errors.New("not a valid token")
	}

	//Convert the body to type string
	// sb := string(body)
	// log.Printf(sb)
}

func NewAuthConnector(cfg *config.BigConfig) (*AuthConnector, error) {
	out := AuthConnector{
		VerifyUrl: cfg.AuthServerConfig.ConnectorPath,
	}

	log.Println("out url in created ", out.VerifyUrl)
	return &out, nil

}

type AuthClientServer struct {
	JWT string

	Client *authn.Client

	ClientAuth *AuthConnector

	R chi.Router

	DBEX *DBEX

	Decider *Decider
}

// var jwt1 = `<your test jwt here>`
// var accountID = `<test ID>`
func (acs *AuthClientServer) BuildRoutes(chin chi.Router) chi.Router {
	rot := chi.NewRouter()

	rot.Post("/signup", acs.SignUpHandler)
	rot.Get("/get-info", acs.GetInfoHandeler)
	rot.Get("/get-all-group", acs.GetAllGroupHandler)
	rot.Post("/add-group", acs.AddGroupHandler)
	rot.Get("/get-one-group", acs.GetOneGroupHandler)

	chin.Mount("/client", rot)

	return chin

}
func NewAuthClient(cfg *config.BigConfig, db *db.DB) (*AuthClientServer, error) {

	clientConnector, err := NewAuthConnector(cfg)

	// if err != nil {
	// 	fmt.Println("  failed to connect 2", err)
	// }

	if err != nil {
		fmt.Println("Connector Failed to connect", err)
	}

	out := AuthClientServer{}
	// client, err := authn.NewClient(authn.Config{
	// 	// The AUTHN_URL of your Keratin AuthN server. This will be used to verify tokens created by
	// 	// AuthN, and will also be used for API calls unless PrivateBaseURL is also set.
	// 	Issuer: cfg.AuthServerConfig.Issuer,
	// 	// The domain of your application (no protocol). This domain should be listed in the APP_DOMAINS
	// 	// of your Keratin AuthN server.
	// 	Audience: cfg.AuthServerConfig.Audience,

	// 	// Credentials for AuthN's private endpoints. These will be used to execute admin actions using
	// 	// the Client provided by this library.
	// 	//
	// 	// TIP: make them extra secure in production!
	// 	Username: cfg.AuthServerConfig.Usern,
	// 	Password: cfg.AuthServerConfig.Passw,

	// 	// RECOMMENDED: Send private API calls to AuthN using private network routing. This can be
	// 	// necessary if your environment has a firewall to limit public endpoints.
	// 	//  PrivateBaseURL: "http://private.example.com",
	// })
	fmt.Println(err)

	decider := NewDecider(cfg)

	//	router := out.Routes()

	//debx := NewDBEX(cfg)
	//out.Client = client
	//	out.R = router
	out.JWT = cfg.AuthServerConfig.JWTSecret
	//	out.DBEX = debx
	out.DBEX = &DBEX{DB: db}
	out.Decider = decider
	out.ClientAuth = clientConnector

	return &out, nil

}

// func (acs *AuthClientServer) Routes(re chi.Router) chi.Router {

// 	rot := chi.NewRouter()

// 	rot.Post("/signup", acs.SignUpHandler)
// 	rot.Get("/get-info", acs.GetInfoHandeler)
// 	rot.Get("/get-all-group", acs.GetAllGroupHandler)
// 	rot.Post("/add-group", acs.AddGroupHandler)
// 	rot.Get("/get-one-group", acs.GetOneGroupHandler)

// 	rot.Mount("/sdfas", rot)
// 	// re.Group(func(r chi.Router) {
// 	// 	r.Mount("/authsdf", rot)

// 	// })
// 	re.Handle("/sgs", rot)
// 	//re.Handle( r)
// 	return re

// }
