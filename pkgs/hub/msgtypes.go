package hub

type MSGTYPE int

//app msg types
const (
	NEWDISCOVERYCREATED MSGTYPE = 100
)

//ws msg types
const (
	NEWCONNECTIONMSG MSGTYPE = 300
)

//update for js msg type

const (
	UPDATEALLDISCOVERY MSGTYPE = 500
)
