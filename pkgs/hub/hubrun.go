package hub

import "log"

func (h *Hub) Run(done chan struct{}) {

	log.Println("hub run called")
	//h.PoolChan <- struct{}{}
	h.PoolChan = done

	// h.FromServicesConnector = appconn
	// h.FromWSConnector = wsconn

	h.HWG.Add(1)
	go h.PoolCreator()
	h.HWG.Add(1)
	go func() {
		//h.PoolChan <- struct{}{}
		log.Println("sending msg down")
		for {
			select {
			case <-h.PoolChan:
				h.HWG.Add(1)
				go h.PoolCreator()
			case <-h.CTX.Done():
				h.Shutdown()
			}
			done <- struct{}{}
		}

	}()

	h.Interpretator.IWG.Wait()
}
