package hub

import (
	"context"
	"log"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/qobbysam/dealbase/pkgs/config"
)

//recreate new pool logic
type Hub struct {
	FromWSConnector       *WSConnector
	Interpretator         *Interpretator
	FromServicesConnector *AppConnector
	Updator               *Updator
	SizePool              int
	PoolChan              chan struct{}
	HWG                   sync.WaitGroup
	CTX                   context.Context
}

func (h *Hub) Shutdown() {

}
func (h *Hub) PoolCreator() {

	log.Println("POOool created")
	// ws_Connector := make(chan WSMSG, 10000)

	// app_Connector := make(chan MSGTYPE, 10000)

	//err_chan := make(chan error, 10000)

	done_chan := make(chan struct{}, 10)

	log.Println("calling interpretator run")
	h.HWG.Add(1)
	go h.Interpretator.Run(h.PoolChan)
	h.HWG.Add(1)
	go func() {
		for {
			select {
			case <-done_chan:
				h.PoolChan <- struct{}{}

				// case <-err_chan:
				// 	<-h.CTX.Done()
			}
		}
	}()
	//h.HWG.Done()
}

type Interpretator struct {
	FromWSConnector  *WSConnector
	FromAppConnector *AppConnector
	IWG              sync.WaitGroup
	InteresList      map[MSGTYPE]string
	Updator          *Updator
}

type AppConnector struct {
	Inmsg chan MSGTYPE
}

func (i *Interpretator) WatchRestIncoming(done chan struct{}) {

	for msg := range i.FromAppConnector.Inmsg {
		i.IWG.Add(1)
		go i.InterPretateAppMsg(msg, done)
	}
}

func (i *Interpretator) InterPretateAppMsg(msg MSGTYPE, done chan struct{}) {

	log.Println("message Received, ", msg)
	log.Println("message Received, ", msg)
	log.Println("message Received, ", msg)
	log.Println("message Received, ", msg)
	switch msg {
	case NEWDISCOVERYCREATED:
		i.IWG.Add(1)
		go i.NewDiscoverCreatedHandler(NEWDISCOVERYCREATED)
	}
	//i.IWG.Done()
}

func (i *Interpretator) NewDiscoverCreatedHandler(msg MSGTYPE) {

	log.Println("msg received. :", msg)
	// interest := i.InteresList[msg]

	// msg_to_send := UPDATEALLDISCOVERY

	// i.Updator.UpdateAllInterests(msg_to_send, interest)

	i.IWG.Done()
}

func (i *Interpretator) WatchWsIncoming() {

	for msg := range i.FromWSConnector.Inmsg {
		i.IWG.Add(1)
		go i.InterPretateWSConnection(msg)
	}

}

func (i *Interpretator) Run(done chan struct{}) {

	// i.FromAppConnector = appconn
	// i.FromWSConnector = wsconn
	log.Println("interpretator Run Called")
	i.IWG.Add(1)
	go i.WatchRestIncoming(done)

	i.IWG.Add(1)
	go i.WatchWsIncoming()

	//i.IWG.Wait()
}

func (i *Interpretator) InterPretateWSConnection(conn WSMSG) {

	switch conn.Msg {
	case NEWCONNECTIONMSG:
		i.IWG.Add(1)
		go i.NewConnectionHandler(conn)
	}
	i.IWG.Done()
}

func (i *Interpretator) NewConnectionHandler(msg WSMSG) {

	interest := i.InteresList[msg.Msg]

	i.Updator.RegisterAConnection(interest, msg.Conn)

	i.IWG.Done()
}

//func (i *Interpretator)

type WSMSG struct {
	Conn *websocket.Conn
	Msg  MSGTYPE
}

type Updator struct {
	MU          sync.Mutex
	WSConnector *WSConnector
	Interests   map[string]InterestConnectGroup
	writeWait   time.Duration
}

func (u *Updator) RegisterAConnection(interest string, conn *websocket.Conn) error {

	//check if interest exists

	u.Interests[interest] = append(u.Interests[interest], conn)

	//	group = append(group, conn)

	return nil

}

func (u *Updator) RegisterInterests(key string, group InterestConnectGroup) {}

func (u *Updator) UpdateAllInterests(msg MSGTYPE, interest string) {

	group := u.Interests[interest]

	for _, v := range group {
		v.SetWriteDeadline(time.Now().Add(u.writeWait))
		// if !ok {
		// 	// The hub closed the channel.
		// 	c.conn.WriteMessage(websocket.CloseMessage, []byte{})
		// 	return
		// }

		w, err := v.NextWriter(websocket.TextMessage)
		if err != nil {
			return
		}
		stringmsg := strconv.Itoa(int(msg))
		w.Write([]byte(stringmsg))
	}

}

type InterestConnectGroup []*websocket.Conn

type WSConnector struct {
	Inmsg  chan WSMSG
	OutMsg chan MSGTYPE
}

func NewUpdator(cfg config.BigConfig) *Updator {
	out := Updator{}

	return &out
}

func NewInterPretator(cfg *config.BigConfig, updator *Updator, fromws *WSConnector, fromapp *AppConnector) *Interpretator {
	out := Interpretator{}

	out.IWG = sync.WaitGroup{}
	out.FromWSConnector = fromws
	out.FromAppConnector = fromapp

	return &out
}

func NewHub(cfg *config.BigConfig, fromws *WSConnector, fromapp *AppConnector, done chan struct{}) *Hub {

	out := Hub{}
	updator := NewUpdator(*cfg)

	interpretator := NewInterPretator(cfg, updator, fromws, fromapp)

	out.FromWSConnector = fromws
	out.FromServicesConnector = fromapp

	out.Updator = updator
	out.Interpretator = interpretator
	out.HWG = sync.WaitGroup{}
	out.CTX = context.Background()
	out.PoolChan = done

	return &out

}
