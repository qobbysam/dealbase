package subrain

import (
	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/logging"
)

type SuBrain struct {
}

func (su *SuBrain) Init() {

}

func (su *SuBrain) BootUp() {

}

func NewSuBrain(cfg *config.BigConfig, logger *logging.BigLogger) *SuBrain {

	return &SuBrain{}
}
