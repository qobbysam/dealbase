package logging

import (
	"go.uber.org/zap"
)

type BigLogger struct {
	logger          *zap.Logger
	DBLogger        *DBLogger
	RestLogger      *RestLogger
	WebsocketLogger *WebsocketLogger
	SuBrainLogger   *SuBrainLogger
	AppBrainLogger  *AppBrainLogger
}

func (bl *BigLogger) WriteInfo(msg string) {
	bl.logger.Info(msg)
}

func (bl *BigLogger) WriteWarn(msg string) {
	bl.logger.Warn(msg)
}

func (bl *BigLogger) WriteDebug(msg string) {
	bl.logger.Debug(msg)
}

func (bl *BigLogger) WriteError(msg string) {
	bl.logger.Error(msg)
}

type DBLogger struct {
	Logger *zap.Logger
}

func (dbl *DBLogger) WriteInfo(msg string) {
	dbl.Logger.Info(msg)
}

func (dbl *DBLogger) WriteWarn(msg string) {
	dbl.Logger.Warn(msg)
}

func (dbl *DBLogger) WriteDebug(msg string) {
	dbl.Logger.Debug(msg)
}

func (dbl *DBLogger) WriteError(msg string) {
	dbl.Logger.Error(msg)
}

type RestLogger struct {
	Logger *zap.Logger
}

func (rl *RestLogger) WriteInfo(msg string) {
	rl.Logger.Info(msg)
}

func (rl *RestLogger) WriteWarn(msg string) {
	rl.Logger.Warn(msg)
}

func (rl *RestLogger) WriteDebug(msg string) {
	rl.Logger.Debug(msg)
}

func (rl *RestLogger) WriteError(msg string) {
	rl.Logger.Error(msg)
}

type WebsocketLogger struct {
	Logger *zap.Logger
}

func (wsl *WebsocketLogger) WriteInfo(msg string) {
	wsl.Logger.Info(msg)
}

func (wsl *WebsocketLogger) WriteWarn(msg string) {
	wsl.Logger.Warn(msg)
}

func (wsl *WebsocketLogger) WriteDebug(msg string) {
	wsl.Logger.Debug(msg)
}

func (wsl *WebsocketLogger) WriteError(msg string) {
	wsl.Logger.Error(msg)
}

type SuBrainLogger struct {
	Logger *zap.Logger
}

func (apl *SuBrainLogger) WriteInfo(msg string) {
	apl.Logger.Info(msg)
}

func (apl *SuBrainLogger) WriteWarn(msg string) {
	apl.Logger.Warn(msg)
}

func (apl *SuBrainLogger) WriteDebug(msg string) {
	apl.Logger.Debug(msg)
}

func (apl *SuBrainLogger) WriteError(msg string) {
	apl.Logger.Error(msg)
}

type AppBrainLogger struct {
	Logger *zap.Logger
}

func (apl *AppBrainLogger) WriteInfo(msg string) {
	apl.Logger.Info(msg)
}

func (apl *AppBrainLogger) WriteWarn(msg string) {
	apl.Logger.Warn(msg)
}

func (apl *AppBrainLogger) WriteDebug(msg string) {
	apl.Logger.Debug(msg)
}

func (apl *AppBrainLogger) WriteError(msg string) {
	apl.Logger.Error(msg)
}

func NewBigLogger() *BigLogger {

	//logger := logrus.New()

	outlogger := BigLogger{}

	locallogger, _ := zap.NewProduction()

	restlogger := locallogger

	wslogger := locallogger

	dblogger := locallogger

	sulogger := locallogger

	applogger := locallogger

	outlogger.logger = locallogger
	outlogger.DBLogger = &DBLogger{Logger: dblogger}
	outlogger.RestLogger = &RestLogger{Logger: restlogger}
	outlogger.WebsocketLogger = &WebsocketLogger{Logger: wslogger}
	outlogger.SuBrainLogger = &SuBrainLogger{Logger: sulogger}
	outlogger.AppBrainLogger = &AppBrainLogger{Logger: applogger}

	outlogger.WriteInfo("Test Logger is working")
	return &outlogger
}

func (bl *BigLogger) GetLogWriter()  {}
func (bl *BigLogger) GetLogEncoder() {}

func (bl *BigLogger) Init() error {

	//bl.WriteInfo("ready in init")

	return nil
}

func (bl *BigLogger) StartApp(stopchan chan struct{}, errorchan chan error) {

	bl.WriteInfo("LoggerStarterd")
}
