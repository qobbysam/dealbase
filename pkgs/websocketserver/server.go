package websocketserver

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/eventstreamhandler"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/logging"
)

type WebSocketServer struct {
	EventHandler *eventstreamhandler.EventStreamHandler
	Logging      *logging.BigLogger
	SharedCtx    context.Context
	StopChan     chan struct{}
	logger       *logging.WebsocketLogger

	Upgrader     *websocket.Upgrader
	Router       chi.Router
	Address      string
	Hub          *hub.Hub
	WSConnector  *hub.WSConnector
	AppConnector *hub.AppConnector
	AppDoneChan  chan struct{}
	WSWG         sync.WaitGroup
}

func (ws *WebSocketServer) Init() error {
	return nil
}

func (ws *WebSocketServer) WatchPool(stopchan chan struct{}, errorchan chan error) {

	// for {
	// 	select {
	// 	case err := <-errorchan:
	// 		msg := fmt.Sprint("error received in pool , ", err)
	// 		ws.logger.WriteError(msg)
	// 		stopchan <- struct{}{}
	// 		//case <- stopchan:

	// 	}
	// }
}

func (ws *WebSocketServer) HandleConn(conn *websocket.Conn) {

	msg := hub.WSMSG{
		Conn: conn,
		Msg:  hub.NEWCONNECTIONMSG,
	}
	ws.WSConnector.Inmsg <- msg

	ws.WSWG.Done()
}

func (ws *WebSocketServer) ReadPump() {}

//func(ws *WebSocketServer) WritePump() {}

func (ws *WebSocketServer) ServeWS(rw http.ResponseWriter, r *http.Request) {
	_, err := ws.Upgrader.Upgrade(rw, r, nil)
	if err != nil {
		msg := fmt.Sprint("failed to start Upgrader server , ", err)
		ws.logger.WriteError(msg)

		//errorchan <- err
	}
}

func (ws *WebSocketServer) BuildRoutes(chin chi.Router) chi.Router {

	ws.Router.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		_, err := ws.Upgrader.Upgrade(rw, r, nil)
		if err != nil {
			msg := fmt.Sprint("failed to start Upgrader server , ", err)
			ws.logger.WriteError(msg)

			//errorchan <- err
		}
	})
	chin.Mount("/ws", ws.Router)

	return chin
}

func (ws *WebSocketServer) StartWSServer(errorchan chan error) {
	http.HandleFunc("/ws", func(rw http.ResponseWriter, r *http.Request) {

		conn, err := ws.Upgrader.Upgrade(rw, r, nil)
		if err != nil {
			msg := fmt.Sprint("failed to start Upgrader server , ", err)
			ws.logger.WriteError(msg)

			errorchan <- err
		}
		ws.WSWG.Add(1)
		go ws.HandleConn(conn)
	})

	err := http.ListenAndServe(ws.Address, nil)

	if err != nil {
		msg := fmt.Sprint("failed to start ws server , ", err)
		ws.logger.WriteError(msg)
		errorchan <- err
	}
}

func (ws *WebSocketServer) Run(appconn *hub.AppConnector, wsconn *hub.WSConnector, done chan struct{}) {
	ws.WSConnector = wsconn
	ws.AppConnector = appconn
	ws.AppDoneChan = done
	ws.WSWG.Add(1)
	go ws.Hub.Run(ws.AppDoneChan)

	ws.Hub.HWG.Wait()
}

func (ws *WebSocketServer) StartApp(stopchan chan struct{}, errorchan chan error) {
	ws.logger.WriteInfo("Websocket Started Succesfully on port " + ws.Address)
	ws.StartWSServer(errorchan)
	ws.WatchPool(stopchan, errorchan)

}

func NewSocketServer(cfg *config.BigConfig, logger *logging.BigLogger, appcon *hub.AppConnector,
	wscon *hub.WSConnector, done chan struct{}, ctx context.Context) *WebSocketServer {

	outserver := WebSocketServer{}

	newhub := hub.NewHub(cfg, wscon, appcon, done)
	outserver.logger = logger.WebsocketLogger
	outserver.Upgrader = &websocket.Upgrader{}
	outserver.Address = cfg.WSServerConfig.Address
	outserver.AppConnector = appcon
	outserver.WSConnector = wscon
	outserver.Hub = newhub
	outserver.AppDoneChan = done
	outserver.Router = chi.NewRouter()

	return &outserver
}
