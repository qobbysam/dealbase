package discovery

import (
	"net/http"

	"github.com/go-chi/render"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
)

type ListDealResponse struct {
	Status int
	Data   []OneDealData
}

func (al *ListDealResponse) Render(w http.ResponseWriter, r *http.Request) error {

	return nil

}

func NewListDealResponse(deals []models.Deal, files []FilesDetail) *ListDealResponse {
	status := http.StatusOK
	onedeal_data := make([]OneDealData, 0)

	for k, v := range deals {

		onedeal_data_v := NewOneDealData(&v, &files[k])
		onedeal_data = append(onedeal_data, *onedeal_data_v)
	}
	return &ListDealResponse{
		Data:   onedeal_data,
		Status: status,
	}
}

type OneDealData struct {
	ID       string         `json:"id"`
	Basic    BasicDetail    `json:"basic"`
	Extended ExtendedDetail `json:"extended"`
	Files    FilesDetail    `json:"files"`
}

type BasicDetail struct {
	Used   bool   `json:"used"`
	Usedby string `json:"useby"`
}

func NewBasicDetail(deal *models.Deal) *BasicDetail {
	return &BasicDetail{
		Used:   deal.BasicDetail.Used,
		Usedby: deal.BasicDetail.UsedBy,
	}
}

type ExtendedDetail struct {
	Name        string `json:"carname"`
	SellerPrice string `json:"carsellerprice"`
	ProfitPrice string `json:"carprofitprice"`
	SoldPrice   string `json:"carsoldprice"`
	Year        string `json:"caryear"`
	Make        string `json:"carmake"`
	Model       string `json:"carmodel"`
	VinNumber   string `json:"carvin"`
}

func NewExtendedDetail(deal *models.Deal) *ExtendedDetail {
	return &ExtendedDetail{
		Name:        deal.ExtendedDetail.Name,
		SellerPrice: deal.ExtendedDetail.SellerPrice,
		ProfitPrice: deal.ExtendedDetail.ProfitPrice,
		SoldPrice:   deal.ExtendedDetail.SoldPrice,
		Year:        deal.ExtendedDetail.Year,
		Make:        deal.ExtendedDetail.Make,
		Model:       deal.ExtendedDetail.Model,
		VinNumber:   deal.ExtendedDetail.VinNumber,
	}
}

type FilesDetail struct {
	Imgs []string `json:"imgs"`
	Pdfs []string `json:"pdfs"`
}

// func NewFiles(deal *models.Deal) *FilesDetail {

// 	imgfiles := deal.Files.ImgsFilesID

// 	pdffiles := deal.Files.PdfFilesID

// 	return &FilesDetail{}
// }

func NewOneDealData(deal *models.Deal, files *FilesDetail) *OneDealData {

	basic := NewBasicDetail(deal)

	extended := NewExtendedDetail(deal)

	//files := NewFiles(deal)

	return &OneDealData{
		ID:       deal.ID,
		Basic:    *basic,
		Extended: *extended,
		Files:    *files,
	}

}

type OneDealResponse struct {
	Status int
	Data   OneDealData
}

func (al *OneDealResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, al.Status)
	return nil

}

func NewOneResponse(deal models.Deal, files *FilesDetail) *OneDealResponse {

	status := http.StatusOK

	data := NewOneDealData(&deal, files)

	return &OneDealResponse{
		Data:   *data,
		Status: status,
	}

}

type ErrResponse struct {
	Err            error `json:"-"` // low-level runtime error
	HTTPStatusCode int   `json:"-"` // http response status code

	StatusText string `json:"status"`          // user-level status message
	AppCode    int64  `json:"code,omitempty"`  // application-specific error code
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

type SuccessResponse struct {
	Msg            string `json:"msg"` // low-level runtime error
	HTTPStatusCode int    `json:"-"`   // http response status code
}

func (e *SuccessResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func NewSuccessResponse(msg string) render.Renderer {
	return &SuccessResponse{
		Msg:            msg,
		HTTPStatusCode: http.StatusOK,
	}
}
