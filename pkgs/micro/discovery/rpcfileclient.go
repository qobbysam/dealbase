package discovery

import (
	"context"
	"log"

	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/smallnest/rpcx/client"
)

type ClientRpc struct {
	Addr            string
	FuncServiceName string
	CallSaveName    string
	CallDeleteName  string
	Client          *client.Peer2PeerDiscovery
}

func NewClientRpc(cfg *config.BigConfig) *ClientRpc {

	out := ClientRpc{
		Addr:            cfg.ConfigRPC.Addr,
		FuncServiceName: cfg.ConfigRPC.FuncServiceName,
		CallSaveName:    cfg.ConfigRPC.CallSaveName,
		CallDeleteName:  cfg.ConfigRPC.CallDeleteName,
	}

	err := out.Init()

	if err != nil {
		log.Println("could not init rpc client")
	}

	return &out

}

func (cc *ClientRpc) Init() error {
	d, err := client.NewPeer2PeerDiscovery("tcp@"+cc.Addr, "")

	if err != nil {

		return err
	}
	cc.Client = d
	return nil
}

func (cc *ClientRpc) SaveImg(upload UploadFileArgs) UploadFileResponse {

	sendargs := upload
	reply := UploadFileResponse{}
	xclient := client.NewXClient(cc.FuncServiceName, client.Failtry, client.RandomSelect, cc.Client, client.DefaultOption)
	err := xclient.Call(context.Background(), cc.CallSaveName, &sendargs, &reply)

	if err != nil {
		log.Println("file saving failed")

		return reply
	}

	log.Println("file upload successfull")

	return reply

}
func (cc *ClientRpc) DeleteImg(delete DeleteFileArgs) DeleteFileResponse {

	sendargs := delete
	reply := DeleteFileResponse{}
	xclient := client.NewXClient("RPCFUNC", client.Failtry, client.RandomSelect, cc.Client, client.DefaultOption)
	err := xclient.Call(context.Background(), "DeleteFile", &sendargs, &reply)

	if err != nil {
		log.Println("file deleting failed")

		return reply
	}

	log.Println("file delete successfull")

	return reply

}

func (cc *ClientRpc) SavePdf() {}

func (cc *ClientRpc) DeletePdf() {

}

type UploadFileArgs struct {
	Type string
	Size int64
	File []byte
}

type UploadFileResponse struct {
	Err  error
	Good bool
	Path string
}

type DeleteFileArgs struct{}

type DeleteFileResponse struct{}
