package discovery

import (
	"log"

	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
)

type DiscoveryDomain struct {
	Mux       chi.Router
	Executor  *DiscoveryExecutor
	ClientRpc *ClientRpc
}

func (dd *DiscoveryDomain) BuildMigrations(dbin *db.DB) error {

	err := dbin.Writer.GormDB.AutoMigrate(
		models.DealDB{}, models.BasicDetail{},
		models.PdfFiles{}, models.ImgsFiles{},
		models.ExtendedDetail{}, models.Files{})

	return err

}

func (dd *DiscoveryDomain) RegisterRestService() (chi.Router, string) {

	err := dd.BuildRoutes()

	if err != nil {
		log.Println("failed to build discover routes", err)
	}
	return dd.Mux, "/discover"

}
func NewDiscoveryDomain(cfg *config.BigConfig, db *db.DB, conn *hub.AppConnector) (*DiscoveryDomain, error) {
	out := DiscoveryDomain{}

	out.Executor = &DiscoveryExecutor{DB: db, Conn: conn}

	out.ClientRpc = NewClientRpc(cfg)

	err := out.BuildMigrations(db)

	if err != nil {

		log.Println("faile to do migrations for discovery")
	}
	return &out, nil
}

func (dd *DiscoveryDomain) BuildRoutes() error {

	router := chi.NewRouter()

	router.Get("/all-deals", dd.AllDealsHandler)

	router.Get("/one-deal", dd.GetOneDealHandler)

	router.Post("/create-one-deal", dd.CreateOneDealHandler)

	router.Post("/create-one-deal-partial", dd.CreateOneDealPartialHandler)

	router.Post("/create-deal-x-endpoint", dd.CreateDealXEndpointHandler)

	router.Delete("/delete-one-deal", dd.DeleteHandler)

	router.Post("/update-one-deal", dd.UpdateHandler)

	router.Post("/upload-one-img", dd.UploadoneImgHandler)

	router.Delete("/delete-one-img", dd.DeleteoneImgHandler)

	router.Post("/upload-one-pdf", dd.UploadonePdfHandler)

	router.Delete("/delete-one-pdf", dd.DeleteonePdfHandler)

	router.Put("/update-deal-img-order", dd.UpdateImgorderHandler)

	out_router := chi.NewRouter()
	out_router.Mount("/discover", router)

	dd.Mux = out_router
	return nil
}
