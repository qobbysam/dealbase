package discovery

import (
	"fmt"
	"log"
	"strings"
	"time"

	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
	"gorm.io/gorm/clause"
)

type DiscoveryExecutor struct {
	DB   *db.DB
	Conn *hub.AppConnector
}

func (de *DiscoveryExecutor) AllDeals() ([]models.DealDB, error) {

	var deals []models.DealDB

	result := de.DB.Reader.GormDB.Order("created desc").Find(&deals)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	return deals, nil
}

func (de *DiscoveryExecutor) ListDBtoDeal(listin []models.DealDB) ([]FilesDetail, []models.Deal, error) {

	out := make([]models.Deal, 0)

	outdetail := make([]FilesDetail, 0)

	for _, v := range listin {

		deal, err := de.BuildDeal(v)

		if err != nil {
			log.Println("failed to build deal ", err)
		}

		out = append(out, *deal)

	}

	for _, v := range listin {

		fils, err := de.GetFiles(v.ID)

		if err != nil {
			outdetail = append(outdetail, FilesDetail{})
			continue
		}

		outdetail = append(outdetail, *fils)

	}
	return outdetail, out, nil
}

func (de *DiscoveryExecutor) GetFilesFromList(modelsdb []models.Deal) ([]FilesDetail, error) {
	outdetail := []FilesDetail{}
	for _, v := range modelsdb {

		fils, err := de.GetFiles(v.ID)

		if err != nil {
			outdetail = append(outdetail, FilesDetail{})
			continue
		}

		outdetail = append(outdetail, *fils)

	}

	return outdetail, nil
}

func (de *DiscoveryExecutor) BuildDeal(dbdeal models.DealDB) (*models.Deal, error) {

	deal := models.Deal{}
	var basicdetail models.BasicDetail
	basicresult := de.DB.Reader.GormDB.Where(&models.BasicDetail{ID: dbdeal.BasicDetailID}).First(&basicdetail)
	if basicresult.Error != nil {
		msg := fmt.Sprint("dberr ", basicresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, basicresult.Error
	}
	var extendeddetail models.ExtendedDetail
	extendedresult := de.DB.Reader.GormDB.Where(&models.ExtendedDetail{ID: dbdeal.ExtendedDetailID}).First(&extendeddetail)
	if extendedresult.Error != nil {
		msg := fmt.Sprint("dberr ", extendedresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, extendedresult.Error
	}
	var filesdetail models.Files
	filesresult := de.DB.Reader.GormDB.Where(&models.Files{ID: dbdeal.FilesID}).First(&filesdetail)
	if filesresult.Error != nil {
		msg := fmt.Sprint("dberr ", filesresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, filesresult.Error
	}
	deal.ID = dbdeal.ID
	deal.BasicDetail = basicdetail

	deal.ExtendedDetail = extendeddetail
	deal.Files = filesdetail

	return &deal, nil
}

func (de *DiscoveryExecutor) UpdateDealImg(deal_in *models.Deal, Imgname string) (string, error) {

	imgfileid := deal_in.Files.ImgsFilesID

	var imgfile models.ImgsFiles

	res := de.DB.Reader.GormDB.Where(&models.ImgsFiles{ID: imgfileid}).First(&imgfile)

	if res.Error != nil {
		return "", res.Error
	}

	filenames := strings.Split(imgfile.FileNames, "***")

	filenames = append(filenames, Imgname)

	joinnames := strings.Join(filenames, "***")

	imgfile.FileNames = joinnames
	update_res := de.DB.Writer.GormDB.Save(&imgfile)
	//deal_in.Files.ImgsFilesID

	if update_res.Error != nil {
		return "", update_res.Error
	}

	return Imgname, nil
}

func (de *DiscoveryExecutor) GetOneDeal(key string) (*models.Deal, error) {

	var dealdb models.DealDB

	//result := de.DB.Reader.GormDB.Where(&deals)

	result := de.DB.Reader.GormDB.Where(&models.DealDB{ID: key}).First(&dealdb)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	deal, err := de.BuildDeal(dealdb)

	if err != nil {
		log.Println("build deal failed")
		return nil, err
	}

	return deal, nil
}

func (de *DiscoveryExecutor) TransFormPartial(partial *models.PartialDeal) *models.Deal {

	return &models.Deal{}
}

func (de *DiscoveryExecutor) CreateOneDeal(deal *models.Deal) (*models.Deal, error) {

	fmt.Println("info in create info  ", deal)
	out := *deal
	// out := Deal{
	// 	ID:       deal.ID,
	// 	//Username: info.Username,
	// 	//	UserProfileID: info.UserProfileID,
	// }
	basicresult := de.DB.Writer.GormDB.Create(&out.BasicDetail)
	if basicresult.Error != nil {
		msg := fmt.Sprint("dberr ", basicresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, basicresult.Error
	}
	extendedresult := de.DB.Writer.GormDB.Create(&out.ExtendedDetail)
	if extendedresult.Error != nil {
		msg := fmt.Sprint("dberr ", extendedresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, extendedresult.Error
	}
	filesresult := de.DB.Writer.GormDB.Create(&out.Files)

	if filesresult.Error != nil {
		msg := fmt.Sprint("dberr ", filesresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, filesresult.Error
	}

	showing := de.DB.Writer.GormDB.Create(&out.ShowingInfo)

	if showing.Error != nil {
		msg := fmt.Sprint("dberr ", showing.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, showing.Error
	}

	staged := de.DB.Writer.GormDB.Create(&out.Staged)

	if staged.Error != nil {
		msg := fmt.Sprint("dberr ", staged.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, staged.Error
	}

	bidding := de.DB.Writer.GormDB.Create(&out.BiddingInfo)

	if bidding.Error != nil {
		msg := fmt.Sprint("dberr ", bidding.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, bidding.Error
	}
	Img := models.ImgsFiles{
		ID: out.Files.ImgsFilesID,
	}
	imgresult := de.DB.Writer.GormDB.Create(&Img)

	if imgresult.Error != nil {
		msg := fmt.Sprint("dberr ", imgresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, imgresult.Error
	}

	Pdf := models.PdfFiles{
		ID: out.Files.PdfFilesID,
	}
	pdfresult := de.DB.Writer.GormDB.Create(&Pdf)

	if pdfresult.Error != nil {
		msg := fmt.Sprint("dberr ", pdfresult.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, pdfresult.Error
	}

	dealdb := models.DealDB{
		ID:               out.ID,
		BasicDetailID:    out.BasicDetail.ID,
		ExtendedDetailID: out.ExtendedDetail.ID,
		FilesID:          out.Files.ID,
		StagedID:         out.Staged.ID,
		ShowingID:        out.ShowingInfo.ID,
		BiddingID:        out.BiddingInfo.ID,
		Created:          time.Now(),
	}
	result := de.DB.Writer.GormDB.Create(&dealdb)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	msg := hub.NEWDISCOVERYCREATED
	de.Conn.Inmsg <- msg

	return &out, nil
}

func (de *DiscoveryExecutor) GetFiles(key string) (*FilesDetail, error) {
	dealb := models.DealDB{}

	dr := de.DB.Reader.GormDB.Where(&models.DealDB{ID: key}).First(&dealb)

	if dr.Error != nil {
		return nil, dr.Error
	}
	filedetail := models.Files{}

	filer := de.DB.Reader.GormDB.Where(&models.Files{ID: dealb.FilesID}).First(&filedetail)

	if filer.Error != nil {
		return nil, filer.Error
	}
	imgdetail := models.ImgsFiles{}
	imgr := de.DB.Reader.GormDB.Where(&models.ImgsFiles{ID: filedetail.ImgsFilesID}).First(&imgdetail)

	if imgr.Error != nil {
		return nil, imgr.Error
	}

	pdfdetail := models.PdfFiles{}

	pdfr := de.DB.Reader.GormDB.Where(&models.PdfFiles{ID: filedetail.PdfFilesID}).First(&pdfdetail)

	if pdfr.Error != nil {
		return nil, pdfr.Error
	}

	imglist := strings.Split(imgdetail.FileNames, "***")

	pdflist := strings.Split(pdfdetail.FileNames, "***")

	return &FilesDetail{
		Imgs: imglist,
		Pdfs: pdflist,
	}, nil

}

func (de *DiscoveryExecutor) CreateOneDealPartial(partialdeal *models.PartialDeal) (*models.Deal, error) {

	outdeal := de.TransFormPartial(partialdeal)

	outs := *outdeal

	result := de.DB.Reader.GormDB.Create(&outs)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	return &outs, nil
}

func (de *DiscoveryExecutor) CreateDealXEndpoint() ([]models.Deal, error) {

	var deals []models.Deal

	result := de.DB.Reader.GormDB.Find(&deals)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	return deals, nil
}

func (e *DiscoveryExecutor) UpdateDeal(deal *models.Deal) error {

	var dbdeal models.DealDB

	fdb := e.DB.Writer.GormDB.Where(&models.DealDB{ID: deal.ID}).First(&dbdeal)

	if fdb.Error != nil {
		log.Println("failed to update", fdb.Error)
		return fdb.Error
	}

	//var extendedInfo models.ExtendedDetail
	// edb := e.DB.Writer.GormDB.Where(&models.ExtendedDetail{ID: deal.ExtendedDetail.ID}).First(&extendedInfo)

	// if edb.Error != nil {
	// 	log.Println("failed to update", edb.Error)
	// 	return edb.Error
	// }

	extendedInfo := deal.ExtendedDetail
	extendedInfo.ID = dbdeal.ExtendedDetailID
	//del := *deal
	sm := e.DB.Writer.GormDB.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&extendedInfo)

	if sm.Error != nil {
		log.Println("failed to update", fdb.Error)
		return sm.Error
	}

	return nil
}
