package discovery

import (
	"errors"
	"net/http"
)

type CreateDealPartialRequest struct{}

func (cdr *CreateDealPartialRequest) Bind(r *http.Request) error {

	return nil
}

type CreateDealRequest struct {
	Name        string `json:"carname"`
	SellerPrice string `json:"carsellerprice"`
	ProfitPrice string `json:"carprofitprice"`
	SoldPrice   string `json:"carsoldprice"`
	Year        string `json:"caryear"`
	Make        string `json:"carmake"`
	Model       string `json:"carmodel"`
	VinNumber   string `json:"carvin"`
}

func (cdr *CreateDealRequest) Bind(r *http.Request) error {

	return nil
}

type UpdateDealRequest struct {
	ID          string `json:"carid"`
	Name        string `json:"carname"`
	SellerPrice string `json:"carsellerprice"`
	ProfitPrice string `json:"carprofitprice"`
	SoldPrice   string `json:"carsoldprice"`
	Year        string `json:"caryear"`
	Make        string `json:"carmake"`
	Model       string `json:"carmodel"`
	VinNumber   string `json:"carvin"`
}

func (cdr *UpdateDealRequest) Bind(r *http.Request) error {

	if cdr.ID == "" {
		return errors.New("ID for Update Cannnot Be empty")
	}
	return nil
}

type CreateDealXEndpointRequest struct{}

func (cdpr *CreateDealXEndpointRequest) Bind(r *http.Request) error {

	return nil
}
