package discovery

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/render"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
)

const MAX_UPLOAD_SIZE int64 = 1024 * 1024 * 3

func (dd *DiscoveryDomain) AllDealsHandler(rw http.ResponseWriter, r *http.Request) {

	dealsdb, err := dd.Executor.AllDeals()

	if err != nil {
		render.Render(rw, r, ErrInvalidRequest(err))
		return
	}

	files, deals, err := dd.Executor.ListDBtoDeal(dealsdb)
	if err != nil {
		render.Render(rw, r, ErrInvalidRequest(err))
		return
	}
	render.Status(r, http.StatusOK)
	render.Render(rw, r, NewListDealResponse(deals, files))

}

func (dd *DiscoveryDomain) GetOneDealHandler(rw http.ResponseWriter, r *http.Request) {

	key := r.URL.Query().Get("id")

	if key == "" {
		render.Render(rw, r, ErrInvalidRequest(errors.New("key cannot be null")))
		return
	}

	deal, err := dd.Executor.GetOneDeal(key)

	if err != nil {
		render.Render(rw, r, ErrInvalidRequest(errors.New("deal does not exist")))
		return
	}

	files, err := dd.Executor.GetFiles(key)

	render.Status(r, http.StatusOK)
	render.Render(rw, r, NewOneResponse(*deal, files))

}

func (dd *DiscoveryDomain) CreateOneDealHandler(rw http.ResponseWriter, r *http.Request) {
	data := &CreateDealRequest{}

	if err := render.Bind(r, data); err != nil {
		render.Render(rw, r, ErrInvalidRequest(err))
		return
	}

	log.Println(data)
	deal := models.NewDeal()

	deal.ExtendedDetail.Name = data.Name
	deal.ExtendedDetail.SellerPrice = data.SellerPrice
	deal.ExtendedDetail.ProfitPrice = data.ProfitPrice

	deal.ExtendedDetail.SoldPrice = data.SoldPrice
	deal.ExtendedDetail.Year = data.Year
	deal.ExtendedDetail.Make = data.Make
	deal.ExtendedDetail.Model = data.Model
	deal.ExtendedDetail.VinNumber = data.VinNumber

	deal_out, err := dd.Executor.CreateOneDeal(deal)

	if err != nil {
		render.Render(rw, r, ErrInvalidRequest(err))
		return
	}

	files, err := dd.Executor.GetFiles(deal_out.ID)

	if err != nil {
		log.Println("failed to pull imgs")
	}

	render.Status(r, http.StatusOK)
	render.Render(rw, r, NewOneResponse(*deal_out, files))

}

func (dd *DiscoveryDomain) CreateOneDealPartialHandler(rw http.ResponseWriter, r *http.Request) {

}

func (dd *DiscoveryDomain) UpdateImgorderHandler(rw http.ResponseWriter, r *http.Request) {

}

func (dd *DiscoveryDomain) UploadoneImgHandler(rw http.ResponseWriter, r *http.Request) {

	//fileUploadRequestData := fileUploadRequestData{}
	//r.Body = http.MaxBytesReader(rw, r.Body, MAX_UPLOAD_SIZE)
	//	r.ParseMultipartForm(10 << 20)

	if err := r.ParseMultipartForm(10 << 20); err != nil {
		http.Error(rw, "The uploaded file is too big. Please choose an file that's less than 1MB in size", http.StatusBadRequest)
		return
	}

	value := r.FormValue("id")

	if value == "" {
		http.Error(rw, "Id cannot be empty", http.StatusBadRequest)
		return
	}

	deal, err := dd.Executor.GetOneDeal(value)
	if err != nil {
		http.Error(rw, "Id does not exist", http.StatusBadRequest)
		return
	}
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}

	defer file.Close()

	//buff := make([]byte, 0)
	buff, err := ioutil.ReadAll(file)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	filetype := http.DetectContentType(buff)
	if filetype != "image/jpeg" && filetype != "image/png" {

		http.Error(rw, "not an png", http.StatusInternalServerError)
		return
	}

	uploadFileArgs := UploadFileArgs{
		Type: "img",
		Size: fileHeader.Size,
		File: buff,
	}

	err_rs := dd.ClientRpc.SaveImg(uploadFileArgs)

	if err_rs.Err != nil {
		http.Error(rw, "Failed to Upload to rpc", http.StatusInternalServerError)
	}

	out, err := dd.Executor.UpdateDealImg(deal, err_rs.Path)

	if err != nil {
		log.Println("failed to update ", out)
		http.Error(rw, "Failed img", http.StatusInternalServerError)
		return
	}

	render.Render(rw, r, NewSuccessResponse("upload successful"))

}

func (dd *DiscoveryDomain) UploadonePdfHandler(rw http.ResponseWriter, r *http.Request) {

}

func (dd *DiscoveryDomain) DeleteoneImgHandler(rw http.ResponseWriter, r *http.Request) {

}
func (dd *DiscoveryDomain) DeleteonePdfHandler(rw http.ResponseWriter, r *http.Request) {

}
func (dd *DiscoveryDomain) CreateDealXEndpointHandler(rw http.ResponseWriter, r *http.Request) {

}

func (dd *DiscoveryDomain) UpdateHandler(rw http.ResponseWriter, r *http.Request) {
	data := &UpdateDealRequest{}

	if err := render.Bind(r, data); err != nil {
		render.Render(rw, r, ErrInvalidRequest(err))
		return
	}
	log.Println(data)

	deal, err := dd.Executor.GetOneDeal(data.ID)

	if err != nil {
		log.Println("get deal ", err)
		http.Error(rw, "Failed to get deal", http.StatusInternalServerError)
		return
	}

	deal.ExtendedDetail.Name = data.Name
	deal.ExtendedDetail.SellerPrice = data.SellerPrice
	deal.ExtendedDetail.ProfitPrice = data.ProfitPrice
	deal.ExtendedDetail.SoldPrice = data.SoldPrice
	deal.ExtendedDetail.VinNumber = data.VinNumber
	deal.ExtendedDetail.Year = data.Year
	deal.ExtendedDetail.Make = data.Make
	deal.ExtendedDetail.Model = data.Model

	err = dd.Executor.UpdateDeal(deal)

	if err != nil {
		log.Println("get update", err)
		http.Error(rw, "Failed Update deal", http.StatusInternalServerError)
		return
	}

	files, err := dd.Executor.GetFiles(deal.ID)

	if err != nil {
		log.Println("failed to pull imgs")
	}

	render.Status(r, http.StatusOK)
	render.Render(rw, r, NewOneResponse(*deal, files))

}

func (dd *DiscoveryDomain) DeleteHandler(rw http.ResponseWriter, r *http.Request) {

}
