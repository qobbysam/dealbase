package staged

import (
	"net/http"
	"strings"

	"github.com/qobbysam/dealbase/pkgs/micro/models"
)

type ListDealResponse struct {
	Status int
	Data   []OneStagedData
}

func (al *ListDealResponse) Render(w http.ResponseWriter, r *http.Request) error {

	return nil

}

func NewListDealResponse(deals []models.StagedDB) *ListDealResponse {
	status := http.StatusOK
	onedeal_data := make([]OneStagedData, 0)

	for _, v := range deals {
		onedeal_data_v := NewOneStagedData(v)
		onedeal_data = append(onedeal_data, *onedeal_data_v)
	}
	return &ListDealResponse{
		Data:   onedeal_data,
		Status: status,
	}
}

type OneStagedData struct {
	ID   string   `json:"id"`
	Name string   `json:"name"`
	Keys []string `json:"keys"`
	//	Files    FilesDetail    `json:"files"`
}

func NewOneStagedData(v models.StagedDB) *OneStagedData {

	return &OneStagedData{
		ID:   v.ID,
		Name: v.Name,
		Keys: strings.Split(v.DealIDList, "***"),
	}
}

func (al *OneStagedData) Render(w http.ResponseWriter, r *http.Request) error {

	return nil

}
