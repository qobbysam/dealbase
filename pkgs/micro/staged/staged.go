package staged

import (
	"log"

	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
	"github.com/qobbysam/dealbase/pkgs/micro/showing"
)

type StagedDomain struct {
	Mux      chi.Router
	Executor *StagedExecutor
	//	ClientRpc *ClientRpc
}

func (dd *StagedDomain) BuildMigrations(dbin *db.DB) error {

	err := dbin.Writer.GormDB.AutoMigrate(&models.StagedDB{}, &models.StagedInfo{})

	return err

}

func (dd *StagedDomain) RegisterRestService() (chi.Router, string) {

	err := dd.BuildRoutes()

	if err != nil {
		log.Println("failed to build discover routes", err)
	}
	return dd.Mux, "/staged"

}
func NewStagedDomain(cfg *config.BigConfig, db *db.DB, conn *hub.AppConnector, DE *discovery.DiscoveryExecutor, SE *showing.ShowingExecutor) (*StagedDomain, error) {
	out := StagedDomain{}

	out.Executor = &StagedExecutor{DB: db, Conn: conn, DE: DE, SE: SE}

	//out.ClientRpc = NewClientRpc(cfg)

	err := out.BuildMigrations(db)

	if err != nil {

		log.Println("faile to do migrations for discovery")
	}
	return &out, nil
}

func (dd *StagedDomain) BuildRoutes() error {

	router := chi.NewRouter()

	router.Get("/all-staged", dd.AllStagedHandler)

	router.Get("/get-staged-detail", dd.StagedDetailHandler)
	router.Post("/create-staged", dd.CreateStagedHandler)

	router.Post("/filter-staged", dd.FilterStagedHandler)

	out_router := chi.NewRouter()
	out_router.Mount("/staged", router)

	dd.Mux = out_router
	return nil
}
