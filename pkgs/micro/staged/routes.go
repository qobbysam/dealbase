package staged

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/go-chi/render"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
)

func (dd *StagedDomain) AllStagedHandler(rw http.ResponseWriter, r *http.Request) {

	allstaged, err := dd.Executor.AllStaged()

	if err != nil {
		render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("bad db action")))
		return
	}

	render.Status(r, http.StatusOK)
	render.Render(rw, r, NewListDealResponse(allstaged))

}

func (dd *StagedDomain) CreateStagedHandler(rw http.ResponseWriter, r *http.Request) {
	data := &CreateStagedRequest{}

	if err := render.Bind(r, data); err != nil {
		render.Render(rw, r, discovery.ErrInvalidRequest(err))
		return
	}

	log.Println(data)

	staged, err := dd.Executor.CreateStaged(*data)

	if err != nil {
		render.Render(rw, r, discovery.ErrInvalidRequest(err))
		return
	}

	render.Status(r, http.StatusOK)
	render.Render(rw, r, NewOneStagedData(*staged))

}

func (dd *StagedDomain) FilterStagedHandler(rw http.ResponseWriter, r *http.Request) {

}

func (dd *StagedDomain) StagedDetailHandler(rw http.ResponseWriter, r *http.Request) {

	key := r.URL.Query().Get("id")
	if key == "" {
		render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("key cannot be null")))
		return
	}

	stagedlist, err := dd.Executor.GetOneStaged(key)

	if err != nil {
		render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("key cannot be null")))
		return
	}

	keys := strings.Split(stagedlist.DealIDList, "***")

	showingdeals, err := dd.Executor.GetListDeals(keys)
	if err != nil {
		render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("keys are missing")))
		return
	}
	files, err := dd.Executor.DE.GetFilesFromList(showingdeals)

	if err != nil {
		//render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("Keys are missing")))
		//return

		log.Println("failed to get files")
	}

	render.Status(r, http.StatusOK)
	render.Render(rw, r, discovery.NewListDealResponse(showingdeals, files))

}
