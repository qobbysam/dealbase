package staged

import "net/http"

type CreateStagedRequest struct {
	Name string   `json:"name"`
	Keys []string `json:"keys"`
	Rule string   `json:"rule"`
}

func (cdr *CreateStagedRequest) Bind(r *http.Request) error {

	return nil
}
