package staged

import (
	"fmt"
	"log"
	"strings"

	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
	"github.com/qobbysam/dealbase/pkgs/micro/showing"
	"gorm.io/gorm/clause"
)

type StagedExecutor struct {
	DB   *db.DB
	Conn *hub.AppConnector
	DE   *discovery.DiscoveryExecutor
	SE   *showing.ShowingExecutor
}

func (se *StagedExecutor) CreateStaged(res CreateStagedRequest) (*models.StagedDB, error) {

	staged := models.NewStagedDB()

	staged.Name = res.Name
	staged.DealIDList = strings.Join(res.Keys, "***")

	reserr := se.DB.Writer.GormDB.Create(&staged)

	if reserr.Error != nil {

		log.Println("failed to save staged")

		return nil, reserr.Error
	}

	err := se.PutKeysInDeal(res.Keys, staged.ID)

	switch res.Rule {
	case "rule1":
		err = se.PutKeysInRoom(res.Keys, "ROOM1")

	case "rule2":
		err = se.PutKeysInRoom(res.Keys, "ROOM2")
	}
	return staged, err
}

func (se *StagedExecutor) PutKeysInDeal(dealsdblist []string, stagedIDkey string) error {

	var listdealdb []models.DealDB

	re := se.DB.Reader.GormDB.Where("id IN ?", dealsdblist).Find(&listdealdb)

	if re.Error != nil {

		log.Println("db execution failed")

		return re.Error
	}

	var stagedKeys []string

	for _, val := range listdealdb {

		stagedKeys = append(stagedKeys, val.StagedID)
	}

	var stagedlisg []models.StagedInfo

	see := se.DB.Reader.GormDB.Where("id IN ?", stagedKeys).Find(&stagedlisg)

	if see.Error != nil {

		log.Println("db execution failed")

		return see.Error
	}

	log.Println("staged lisg ", len(stagedlisg))
	var modifiedstaged []models.StagedInfo
	for _, val := range stagedlisg {

		list_ := strings.Split(val.StagedList, "***")

		list_ = append(list_, stagedIDkey)

		joined := strings.Join(list_, "***")

		val.StagedList = joined

		modifiedstaged = append(modifiedstaged, val)
	}

	sm := se.DB.Writer.GormDB.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&modifiedstaged)

	if sm.Error != nil {
		log.Println("failed to update", sm.Error)
		return sm.Error
	}

	return nil

}

func (se *StagedExecutor) PutKeysInRoom(keys []string, roomid string) error {
	var listdealdb []models.DealDB

	re := se.DB.Reader.GormDB.Where("id IN ?", keys).Find(&listdealdb)

	if re.Error != nil {

		log.Println("db execution failed")

		return re.Error
	}

	var showingidkeys []string

	for _, v := range listdealdb {
		showingidkeys = append(showingidkeys, v.ShowingID)
	}

	var showIds []models.ShowInfo

	sre := se.DB.Reader.GormDB.Where("id IN ?", showingidkeys).Find(&showIds)

	if sre.Error != nil {

		log.Println("db execution failed")

		return sre.Error
	}

	var modifiedshowing []models.ShowInfo
	for _, val := range showIds {

		list_ := strings.Split(val.RoomIDList, "***")

		list_ = append(list_, roomid)

		joined := strings.Join(list_, "***")

		val.RoomIDList = joined

		modifiedshowing = append(modifiedshowing, val)
	}

	sm := se.DB.Writer.GormDB.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&modifiedshowing)

	if sm.Error != nil {
		log.Println("failed to update", sm.Error)
		return sm.Error
	}

	return sm.Error

}

func (se *StagedExecutor) GetOneStaged(key string) (*models.StagedDB, error) {

	var staged models.StagedDB

	//out_Deals := []models.Deal{}

	res := se.DB.Reader.GormDB.Where(&models.StagedDB{ID: key}).First(&staged)

	if res.Error != nil {

		log.Println("did not find staged")

		return nil, res.Error
	}

	//split_keys := strings.Split(staged.DealIDList, "***")

	// for _, v := range split_keys {

	// 	deal, err := se.DE.GetOneDeal(v)

	// 	if err != nil {
	// 		log.Println("deal does not exist")
	// 		continue
	// 	}

	// 	out_Deals = append(out_Deals, *deal)
	// }

	return &staged, nil
}

func (de *StagedExecutor) AllStaged() ([]models.StagedDB, error) {

	var staged []models.StagedDB

	result := de.DB.Reader.GormDB.Order("created desc").Find(&staged)

	if result.Error != nil {
		msg := fmt.Sprint("dberr ", result.Error)
		de.DB.Writer.Logger.WriteInfo(msg)

		return nil, result.Error
	}

	return staged, nil
}

func (se *StagedExecutor) BuildDeals(dbin *[]models.DealDB) ([]models.Deal, error) {

	out := []models.Deal{}
	for _, v := range *dbin {

		deal, err := se.DE.BuildDeal(v)

		if err != nil {
			log.Println("failed to build, ", deal)
		}

		out = append(out, *deal)

	}

	return out, nil

}

func (de *StagedExecutor) GetListDeals(keys []string) ([]models.Deal, error) {
	var deals []models.DealDB

	derr := de.DB.Reader.GormDB.Where("id IN ?", keys).Find(&deals)

	if derr.Error != nil {
		log.Println("failed to get keys keys")
	}

	dealist, err := de.BuildDeals(&deals)

	if err != nil {
		log.Println("failed to buld lsist")
	}

	return dealist, nil

}

// func (de *DiscoveryExecutor) ListDBtoDeal(listin []DealDB) ([]Deal, error) {

// 	out := make([]Deal, 0)

// 	for _, v := range listin {

// 		deal, err := de.BuildDeal(v)

// 		if err != nil {
// 			log.Println("failed to build deal ", err)
// 		}

// 		out = append(out, *deal)

// 	}

// 	return out, nil
// }

// func (de *DiscoveryExecutor) BuildDeal(dbdeal DealDB) (*Deal, error) {

// 	deal := Deal{}
// 	var basicdetail models.BasicDetail
// 	basicresult := de.DB.Reader.GormDB.Where(&models.BasicDetail{ID: dbdeal.BasicDetailID}).First(&basicdetail)
// 	if basicresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", basicresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, basicresult.Error
// 	}
// 	var extendeddetail models.ExtendedDetail
// 	extendedresult := de.DB.Reader.GormDB.Where(&models.ExtendedDetail{ID: dbdeal.ExtendedDetailID}).First(&extendeddetail)
// 	if extendedresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", extendedresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, extendedresult.Error
// 	}
// 	var filesdetail models.Files
// 	filesresult := de.DB.Reader.GormDB.Where(&models.Files{ID: dbdeal.FilesID}).First(&filesdetail)
// 	if filesresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", filesresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, filesresult.Error
// 	}
// 	deal.ID = dbdeal.ID
// 	deal.BasicDetail = basicdetail

// 	deal.ExtendedDetail = extendeddetail
// 	deal.Files = filesdetail

// 	return &deal, nil
// }

// func (de *DiscoveryExecutor) UpdateDealImg(deal_in *Deal, Imgname string) (string, error) {

// 	imgfileid := deal_in.Files.ImgsFilesID

// 	var imgfile models.ImgsFiles

// 	res := de.DB.Reader.GormDB.Where(&models.ImgsFiles{ID: imgfileid}).First(&imgfile)

// 	if res.Error != nil {
// 		return "", res.Error
// 	}

// 	filenames := strings.Split(imgfile.FileNames, "***")

// 	filenames = append(filenames, Imgname)

// 	joinnames := strings.Join(filenames, "***")

// 	imgfile.FileNames = joinnames
// 	update_res := de.DB.Writer.GormDB.Save(&imgfile)
// 	//deal_in.Files.ImgsFilesID

// 	if update_res.Error != nil {
// 		return "", update_res.Error
// 	}

// 	return Imgname, nil
// }

// func (de *DiscoveryExecutor) GetOneDeal(key string) (*Deal, error) {

// 	var dealdb DealDB

// 	//result := de.DB.Reader.GormDB.Where(&deals)

// 	result := de.DB.Reader.GormDB.Where(&DealDB{ID: key}).First(&dealdb)

// 	if result.Error != nil {
// 		msg := fmt.Sprint("dberr ", result.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, result.Error
// 	}

// 	deal, err := de.BuildDeal(dealdb)

// 	if err != nil {
// 		log.Println("build deal failed")
// 		return nil, err
// 	}

// 	return deal, nil
// }

// func (de *DiscoveryExecutor) TransFormPartial(partial *PartialDeal) *Deal {

// 	return &Deal{}
// }

// func (de *DiscoveryExecutor) CreateOneDeal(deal *Deal) (*Deal, error) {

// 	fmt.Println("info in create info  ", deal)
// 	out := *deal
// 	// out := Deal{
// 	// 	ID:       deal.ID,
// 	// 	//Username: info.Username,
// 	// 	//	UserProfileID: info.UserProfileID,
// 	// }
// 	basicresult := de.DB.Writer.GormDB.Create(&out.BasicDetail)
// 	if basicresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", basicresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, basicresult.Error
// 	}
// 	extendedresult := de.DB.Writer.GormDB.Create(&out.ExtendedDetail)
// 	if extendedresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", extendedresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, extendedresult.Error
// 	}
// 	filesresult := de.DB.Writer.GormDB.Create(&out.Files)

// 	if filesresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", filesresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, filesresult.Error
// 	}

// 	Img := models.ImgsFiles{
// 		ID: out.Files.ImgsFilesID,
// 	}
// 	imgresult := de.DB.Writer.GormDB.Create(&Img)

// 	if imgresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", imgresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, imgresult.Error
// 	}

// 	Pdf := models.PdfFiles{
// 		ID: out.Files.PdfFilesID,
// 	}
// 	pdfresult := de.DB.Writer.GormDB.Create(&Pdf)

// 	if pdfresult.Error != nil {
// 		msg := fmt.Sprint("dberr ", pdfresult.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, pdfresult.Error
// 	}

// 	dealdb := DealDB{
// 		ID:               out.ID,
// 		BasicDetailID:    out.BasicDetail.ID,
// 		ExtendedDetailID: out.ExtendedDetail.ID,
// 		FilesID:          out.Files.ID,
// 		Created:          time.Now(),
// 	}
// 	result := de.DB.Writer.GormDB.Create(&dealdb)

// 	if result.Error != nil {
// 		msg := fmt.Sprint("dberr ", result.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, result.Error
// 	}

// 	msg := hub.NEWDISCOVERYCREATED
// 	de.Conn.Inmsg <- msg

// 	return &out, nil
// }

// func (de *DiscoveryExecutor) CreateOneDealPartial(partialdeal *PartialDeal) (*Deal, error) {

// 	outdeal := de.TransFormPartial(partialdeal)

// 	outs := *outdeal

// 	result := de.DB.Reader.GormDB.Create(&outs)

// 	if result.Error != nil {
// 		msg := fmt.Sprint("dberr ", result.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, result.Error
// 	}

// 	return &outs, nil
// }

// func (de *DiscoveryExecutor) CreateDealXEndpoint() ([]Deal, error) {

// 	var deals []Deal

// 	result := de.DB.Reader.GormDB.Find(&deals)

// 	if result.Error != nil {
// 		msg := fmt.Sprint("dberr ", result.Error)
// 		de.DB.Writer.Logger.WriteInfo(msg)

// 		return nil, result.Error
// 	}

// 	return deals, nil
// }
