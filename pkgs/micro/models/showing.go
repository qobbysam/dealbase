package models

import "github.com/lithammer/shortuuid"

type ShowInfo struct {
	ID         string `gorm:"primaryKey" ,json:""`
	RoomIDList string
}

type ShowRoom struct {
	ID   string `gorm:"primaryKey" ,json:""`
	Name string
}

func NewShowingInfo() *ShowInfo {
	return &ShowInfo{
		ID: shortuuid.New(),
	}
}
