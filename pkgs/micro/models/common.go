package models

import (
	"github.com/lithammer/shortuuid"
)

//files [0] imgs [1]pdf

type BasicDetail struct {
	ID     string `gorm:"primaryKey" ,json:""`
	Used   bool   `json:"used"`
	UsedBy string `json:"usedby"`
}

type ExtendedDetail struct {
	ID          string `gorm:"primaryKey" ,json:"key"`
	Name        string `json:"name"`
	SellerPrice string `json:"sellerprice"`
	ProfitPrice string `json:"profitprice"`
	SoldPrice   string `json:"soldprice"`
	Year        string `json:"year"`
	Make        string `json:"make"`
	Model       string `json:"model"`
	VinNumber   string `json:"vin"`
}

type Files struct {
	ID          string `gorm:"primaryKey" ,json:"key"`
	PdfFilesID  string `json:"pdfid"`
	ImgsFilesID string `json:"imgid"`
}

// func (f Files) Scan(value interface{}) error {
// 	return nil
// }
// func (f Files) Value() (driver.Value, error) {

// 	return json.RawMessage(f.ImgsFilesID).MarshalJSON()
// }

type PdfFiles struct {
	ID string `gorm:"primaryKey" ,json:"key"`

	Type      string `json:"type"`
	Url       string `json:"url"`
	FileNames string `json:"filenames"`
	FileUrls  string `json:"fileurls"`
}

type ImgsFiles struct {
	ID        string `gorm:"primaryKey" ,json:"key"`
	Type      string `json:"type"`
	Url       string `json:"url"`
	FileNames string `json:"filenames"`
	FileUrls  string `json:"fileurls"`
}

// type OneFile struct {
// 	Type string `json:"type"`
// 	Url  string `json:"url"`
// }

func NewBasicDetail() *BasicDetail {

	return &BasicDetail{
		ID: shortuuid.New(),
	}
}

func NewExtendedDetail() *ExtendedDetail {
	return &ExtendedDetail{
		ID: shortuuid.New(),
	}
}

func NewFiles() *Files {

	pdffiles := PdfFiles{ID: shortuuid.New()}

	imgfiles := ImgsFiles{ID: shortuuid.New()}

	return &Files{ID: shortuuid.New(), PdfFilesID: pdffiles.ID, ImgsFilesID: imgfiles.ID}
}

// func NewOneFile() *OneFile {

// 	return &OneFile{
// 		//ID: shortuuid.New(),
// 	}
// }
