package models

import (
	"time"

	"github.com/lithammer/shortuuid"
)

type StagedDB struct {
	ID         string
	Name       string
	DealIDList string
	Created    time.Time
}

type StagedInfo struct {
	ID         string
	StagedList string
}

func NewStagedDB() *StagedDB {

	return &StagedDB{ID: shortuuid.New(), Created: time.Now()}
}

func NewStagedInfo() *StagedInfo {

	return &StagedInfo{
		ID: shortuuid.New(),
	}
}
