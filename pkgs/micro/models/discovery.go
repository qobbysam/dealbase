package models

import (
	"time"

	"github.com/lithammer/shortuuid"
)

type Deal struct {
	ID             string         `gorm:"primaryKey" ,json:""`
	BasicDetail    BasicDetail    `json:""`
	ExtendedDetail ExtendedDetail `json:""`
	Files          Files          `json:""`
	Staged         StagedInfo
	ShowingInfo    ShowInfo
	BiddingInfo    BiddingInfo
	//Staged      staged.StagedDB
	//FilesID          string
	//BasicDetailID    string
	//	ExtendedDetailID string
}

type DealDB struct {
	ID               string `gorm:"primaryKey" ,json:""`
	BasicDetailID    string
	ExtendedDetailID string `json:""`
	FilesID          string `json:""`
	Created          time.Time
	StagedID         string
	ShowingID        string
	BiddingID        string
	//FilesID          string
	//BasicDetailID    string
	//	ExtendedDetailID string
}

type PartialDeal struct{}

func NewDeal() *Deal {
	out := Deal{}

	basicdetail := NewBasicDetail()
	extendedDetail := NewExtendedDetail()

	showing := NewShowingInfo()

	staging := NewStagedInfo()

	bidding := NewBiddingInfo()
	//stagedid := staged.NewStagedDB()

	files := NewFiles()

	out.ID = shortuuid.New()

	out.BasicDetail = *basicdetail
	out.ExtendedDetail = *extendedDetail
	out.Files = *files

	out.Staged = *staging
	out.ShowingInfo = *showing
	out.BiddingInfo = *bidding

	//	out.

	return &out

}
