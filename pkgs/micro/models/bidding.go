package models

import (
	"time"

	"github.com/lithammer/shortuuid"
)

type BiddingInfo struct {
	ID        string
	Active    bool
	StartTime time.Time
	EndTime   time.Time
	RoomID    string
}

type BiddingRoom struct {
	ID   string
	Name string
}

func NewBiddingInfo() *BiddingInfo {
	return &BiddingInfo{
		ID: shortuuid.New(),
	}
}
