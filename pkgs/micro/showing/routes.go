package showing

import (
	"errors"
	"net/http"

	"github.com/go-chi/render"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
)

func (dd *ShowingDomain) AllRoomManagerHandler(rw http.ResponseWriter, r *http.Request) {

	key := r.URL.Query().Get("roomid")

	if key == "" {
		render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("roomid cannot be empty")))
		return
	}

	showingdeals, err := dd.Executor.RoomDeals(key)

	if err != nil {
		render.Render(rw, r, discovery.ErrInvalidRequest(errors.New("bad request")))
		return
	}

	files, err := dd.Executor.DE.GetFilesFromList(showingdeals)

	render.Status(r, http.StatusOK)
	render.Render(rw, r, discovery.NewListDealResponse(showingdeals, files))

}

func (dd *ShowingDomain) AllRoomBuyerHandler(rw http.ResponseWriter, r *http.Request) {

}
