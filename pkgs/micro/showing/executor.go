package showing

import (
	"log"

	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
)

type ShowingExecutor struct {
	DB   *db.DB
	Conn *hub.AppConnector
	DE   *discovery.DiscoveryExecutor
}

func (se *ShowingExecutor) BuildIds(infos *[]models.ShowInfo) []string {

	out := []string{}

	for _, v := range *infos {

		out = append(out, v.ID)
	}

	return out

}

func (se *ShowingExecutor) BuildDeals(dbin *[]models.DealDB) ([]models.Deal, error) {

	out := []models.Deal{}
	for _, v := range *dbin {

		deal, err := se.DE.BuildDeal(v)

		if err != nil {
			log.Println("failed to build, ", deal)
		}

		out = append(out, *deal)

	}

	return out, nil

}

func (se *ShowingExecutor) RoomDeals(key string) ([]models.Deal, error) {

	//var out []discovery.DealDB

	var showinging []models.ShowInfo

	querystring := "%" + key + "%"
	rerr := se.DB.Reader.GormDB.Where("room_id_list LIKE ?", querystring).Find(&showinging)

	if rerr.Error != nil {

		log.Println("failed to get all showing room")
	}

	ids := se.BuildIds(&showinging)

	var deals []models.DealDB

	derr := se.DB.Reader.GormDB.Where("showing_id IN ?", ids).Order("created desc").Find(&deals)

	if derr.Error != nil {
		log.Println("failed to get room keys")
	}

	dealist, err := se.BuildDeals(&deals)

	if err != nil {
		log.Println("failed to buld list")
	}

	//deals := se.DE.BuildDeal()

	return dealist, nil

	//err := DB.Reader.GormDB.

}
