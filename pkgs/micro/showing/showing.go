package showing

import (
	"log"

	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
)

type ShowingDomain struct {
	Mux      chi.Router
	Executor *ShowingExecutor
	//	ClientRpc *ClientRpc
}

func (dd *ShowingDomain) BuildMigrations(dbin *db.DB) error {

	err := dbin.Writer.GormDB.AutoMigrate(&models.ShowInfo{}, &models.ShowRoom{}, &models.BiddingInfo{})

	return err

}

func (dd *ShowingDomain) RegisterRestService() (chi.Router, string) {

	err := dd.BuildRoutes()

	if err != nil {
		log.Println("failed to build discover routes", err)
	}
	return dd.Mux, "/showing"

}
func NewShowingDomain(cfg *config.BigConfig, db *db.DB, conn *hub.AppConnector, DE *discovery.DiscoveryExecutor) (*ShowingDomain, error) {
	out := ShowingDomain{}

	out.Executor = &ShowingExecutor{DB: db, Conn: conn, DE: DE}

	//out.ClientRpc = NewClientRpc(cfg)

	err := out.BuildMigrations(db)

	if err != nil {

		log.Println("faile to do migrations for discovery")
	}
	return &out, nil
}

func (dd *ShowingDomain) BuildRoutes() error {

	router := chi.NewRouter()

	router.Get("/all-room-manager", dd.AllRoomManagerHandler)

	router.Get("/all-room-buyer", dd.AllRoomBuyerHandler)
	// router.Get("/get-staged-detail", dd.StagedDetailHandler)
	// router.Post("/create-staged", dd.CreateStagedHandler)

	// router.Post("/filter-staged", dd.FilterStagedHandler)

	out_router := chi.NewRouter()
	out_router.Mount("/showing", router)

	dd.Mux = out_router
	return nil
}
