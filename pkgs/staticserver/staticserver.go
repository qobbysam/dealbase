package staticserver

import (
	"net/http"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/config"
)

type StaticServer struct {
	Router    chi.Router
	StaticDir string
}

func (st *StaticServer) BuildRoutes(chin chi.Router) chi.Router {
	chin.Mount("/", st.Router)

	return chin

}

func NewStaticServer(cfg *config.BigConfig) *StaticServer {
	outserver := StaticServer{}

	staticdir := filepath.Join(cfg.StaticServerConfig.BaseDir, cfg.StaticServerConfig.StaticDir)

	outserver.StaticDir = staticdir

	// out.Directory = cfg.StaticServer.FilePath
	// out.Port = cfg.StaticServer.Port
	fs := http.FileServer(http.Dir(outserver.StaticDir))
	outserver.Router = chi.NewRouter()
	outserver.Router.Handle("/*", fs)
	//out.R.Get("/", out.ServeStatic)

	//fmt.Println(out.Path)

	//return &out, nil
	return &outserver
}
