package restserver

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/go-chi/chi"
	"github.com/qobbysam/dealbase/pkgs/auth"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/iface"
	"github.com/qobbysam/dealbase/pkgs/logging"
	"github.com/qobbysam/dealbase/pkgs/micro/discovery"
	"github.com/qobbysam/dealbase/pkgs/micro/showing"
	"github.com/qobbysam/dealbase/pkgs/micro/staged"
	"github.com/qobbysam/dealbase/pkgs/restservicehandler"
)

type RestServer struct {
	RestHandler *restservicehandler.RestServiceHandler
	Router      chi.Router
	SharedCtx   context.Context
	Logger      *logging.RestLogger
	StopChan    chan struct{}
	RestIface   []iface.RestServiceInterface
	Config      *config.BigConfig
	DBconn      *db.DB

	Appconnector *hub.AppConnector

	Address string
}

func (rs *RestServer) BuildRoutes(chin chi.Router) chi.Router {

	var routesmap = map[string]chi.Router{}

	for _, v := range rs.RestIface {

		routerr, stringss := v.RegisterRestService()
		routesmap[stringss] = routerr
		//routes.Connect(stringss, routerr)

		// if err != nil {
		// 	fmt.Println("error in regist routes, ", err)
		// }
		//router.Handle(pattern, route)
		// router.Group(func(r chi.Router) {
		// 	r.Mount(pattern, route)
		// })
	}

	routes := chi.NewRouter()

	for k, v := range routesmap {
		routes.Mount(k, v)
	}

	chin.Mount("/api", routes)

	return chin
}

// func (rs *RestServer) Routes(r chi.Router) chi.Router {

// 	router := chi.NewRouter()

// 	//router.Mount("/rest", )

// 	for _, v := range rs.RestIface {

// 		routerr, stringss := v.RegisterRestService()

// 		router.Mount(stringss, routerr)

// 		// if err != nil {
// 		// 	fmt.Println("error in regist routes, ", err)
// 		// }
// 		//router.Handle(pattern, route)
// 		// router.Group(func(r chi.Router) {
// 		// 	r.Mount(pattern, route)
// 		// })
// 	}
// 	r.Mount("/sfs", router)
// 	rs.Router = r

// 	//router.Mount("")

// 	return router
// }

func (rs *RestServer) RegisterRoutes() {

}

func (rs *RestServer) Init() error {
	err := BuildRestServer(rs)
	//	rs.RegisterRoutes()

	return err
}

func (rs *RestServer) RegisterARouter(path string, router chi.Router) {

	rs.Router.Handle(path, router)

	fmt.Println("Routes Registered")
}
func (rs *RestServer) WatchPool(stopchan chan struct{}, errorchan chan error) {
	for s := range errorchan {
		msg := fmt.Sprint("failed to start Rest server , ", s)
		rs.Logger.WriteError(msg)
		stopchan <- struct{}{}

	}
}

func (rs *RestServer) ServeRestServer(errorchan chan error, wg sync.WaitGroup) {

	err := http.ListenAndServe(rs.Address, rs.Router)

	if err != nil {
		msg := fmt.Sprint("failed to start Rest server , ", err)
		rs.Logger.WriteError(msg)
		errorchan <- err
		wg.Done()
	}
}

func (rs *RestServer) StartApp(stopchan chan struct{}, errorchan chan error) {
	var wg sync.WaitGroup

	rs.Logger.WriteInfo("restserver started on port , " + rs.Address)
	wg.Add(1)
	go func() {
		rs.ServeRestServer(errorchan, wg)
		rs.WatchPool(stopchan, errorchan)
	}()

}

func NewRestServer(cfg *config.BigConfig, logger *logging.BigLogger, dbconn *db.DB, appcon *hub.AppConnector, ctx context.Context) *RestServer {
	outserver := RestServer{}
	//router_ := outserver.Routes()
	outserver.Logger = logger.RestLogger
	//outserver.Router = router_
	outserver.Address = cfg.RestServerConfig.Address
	outserver.Config = cfg
	outserver.DBconn = dbconn
	outserver.Appconnector = appcon

	//router.Handle("", router_)

	err := outserver.Init()

	if err != nil {
		fmt.Println("error in rest server", err)
	}

	return &outserver
}

// func ServeRestServer()

func BuildRestServer(rs *RestServer) error {

	authserver := auth.NewAuthObject(rs.Config, rs.DBconn, rs.Logger)
	rs.RestIface = append(rs.RestIface, authserver)
	discovery, err := discovery.NewDiscoveryDomain(rs.Config, rs.DBconn, rs.Appconnector)

	if err != nil {
		log.Println("failed to init discovery")
	}
	//err := authserver.Init()
	rs.RestIface = append(rs.RestIface, discovery)

	showing, err := showing.NewShowingDomain(rs.Config, rs.DBconn, rs.Appconnector, discovery.Executor)

	if err != nil {
		log.Println("failed to init discovery")
	}

	rs.RestIface = append(rs.RestIface, showing)

	staged, err := staged.NewStagedDomain(rs.Config, rs.DBconn, rs.Appconnector, discovery.Executor, showing.Executor)
	if err != nil {
		log.Println("failed to init discovery")
	}

	rs.RestIface = append(rs.RestIface, staged)

	fmt.Println("built rest services")

	return nil
}
