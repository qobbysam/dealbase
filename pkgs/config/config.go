package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

type BigConfig struct {
	RestServerConfig    *RestServerConfig    `json:"restconfig"`
	WSServerConfig      *WSServerConfig      `json:"wssconfig"`
	StaticServerConfig  *StaticServerConfig  `json:"staticserverconfig"`
	Use                 string               `yml:"use" ,json:"use"`
	AuthServerConfig    *AuthServerConfig    `json:"authserver"`
	DBWriterConfig      *DBWriterConfig      `json:"dbwriter"`
	DBReaderConfig      *DBReaderConfig      `json:"dbreader"`
	SUBrainConfig       *SUBrainConfig       `json:"subrain"`
	AppBrainConfig      *AppBrainConfig      `json:"appbrain"`
	FileStorageConfig   *FileStorageConfig   `json:"filestorage"`
	NotificationsConfig *NotificationsConfig `json:"notifications"`
	NatsConfig          *NatsConfig          `json:"nats"`
	EventDBConfig       *EventDBConfig       `json:"eventdb"`

	ConfigRPC *ConfigRPC `json:"configrpc"`
}

type RestServerConfig struct {
	Address string `yml:"address" ,json:"address"`
}

type WSServerConfig struct {
	Address string `yml:"address" ,json:"address"`
}

type AuthServerConfig struct {
	JWTSecret string `yml:"jwtsecret" ,json:"jwtsecret"`
	//authnclient
	Issuer        string        `yml:"issuer" ,json:"issuer"`
	Audience      string        `yml:"audience" ,json:"audience"`
	Usern         string        `yml:"username" ,json:"username"`
	Passw         string        `yml:"password" ,json:"password"`
	RoutesConfig  *RoutesConfig `yml:"routesconfig" ,json:"routesconfig"`
	ConnectorPath string        `yml:"connectorpath" ,json:"connectorpath"`
}

type RoutesConfig struct {
	ModeratorManagerKey string `yml:"moderatormanagerkey" ,json:"moderatormanagerkey"`
	ModeratorWorkerKey  string `yml:"moderatorworkerkey" ,json:"moderatorworkerkey"`
	DealSellerKey       string `yml:"dealsellerkey" ,json:"dealsellerkey"`
	DealBuyerKey        string `yml:"dealbuyerkey" ,json:"dealbuyerkey"`
	SuSuKey             string `yml:"susukey" ,json:"susukey"`
	AppSuKey            string `yml:"appsukey" ,json:"appsukey"`
}

type PostGresConfig struct {
	DBName     string `yml:"dbname" ,json:"dbname"`
	DBHost     string `yml:"dbhost" ,json:"dbhost"`
	DBPort     string `yml:"dbport" ,json:"dbport"`
	DBUsername string `yml:"dbusername" ,json:"dbusername"`
	DBPassword string `yml:"dbpassword" ,json:"dbpassword"`
}

type SqliteConfig struct {
	DBName string `yml:"dbname" ,json:"dbname"`
}

type DBWriterConfig struct {
	IfaceType      string          `yml:"ifacetype" ,json:"ifacetype"`
	PostGresConfig *PostGresConfig `yml:"postgresconfig" ,json:"postgresconfig"`
	SqliteConfig   *SqliteConfig   `yml:"sqliteconfig" ,json:"sqliteconfig"`
}

type DBReaderConfig struct {
	IfaceType      string          `yml:"ifacetype" ,json:"ifacetype"`
	PostGresConfig *PostGresConfig `yml:"postgresconfig" ,json:"postgresconfig"`
	SqliteConfig   *SqliteConfig   `yml:"sqliteconfig" ,json:"sqliteconfig"`
}

type SUBrainConfig struct {
	Address string `yml:"address" ,json:"address"`
}

type AppBrainConfig struct {
	Address string `yml:"address" ,json:"address"`
}

type FileStorageConfig struct {
	DBName     string `yml:"dbname" ,json:"dbname"`
	DBHost     string `yml:"dbhost" ,json:"dbhost"`
	DBPort     string `yml:"dbport" ,json:"dbport"`
	DBUsername string `yml:"dbusername" ,json:"dbusername"`
	DBPassword string `yml:"dbpassword" ,json:"dbpassword"`
}

type NotificationsConfig struct {
	DBName     string `yml:"dbname" ,json:"dbname"`
	DBHost     string `yml:"dbhost" ,json:"dbhost"`
	DBPort     string `yml:"dbport" ,json:"dbport"`
	DBUsername string `yml:"dbusername" ,json:"dbusername"`
	DBPassword string `yml:"dbpassword" ,json:"dbpassword"`
}

type NatsConfig struct {
	DBName     string `yml:"dbname" ,json:"dbname"`
	DBHost     string `yml:"dbhost" ,json:"dbhost"`
	DBPort     string `yml:"dbport" ,json:"dbport"`
	DBUsername string `yml:"dbusername" ,json:"dbusername"`
	DBPassword string `yml:"dbpassword" ,json:"dbpassword"`
}

type EventDBConfig struct {
	DBName     string `yml:"dbname" ,json:"dbname"`
	DBHost     string `yml:"dbhost" ,json:"dbhost"`
	DBPort     string `yml:"dbport" ,json:"dbport"`
	DBUsername string `yml:"dbusername" ,json:"dbusername"`
	DBPassword string `yml:"dbpassword" ,json:"dbpassword"`
}

type StaticServerConfig struct {
	BaseDir   string `yml:"basedir" ,json:"basedir"`
	StaticDir string `yml:"staticdir" ,json:"staticdir"`
}

type ConfigRPC struct {
	Addr            string `json:"addr"`
	FuncServiceName string `json:"funcservicename"`
	CallSaveName    string `json:"callsavename"`
	CallDeleteName  string `json:"calldeletename"`
}

func (bg *BigConfig) BuildEager() error {
	return nil
}

func (bg *BigConfig) BuildCluster() error {
	return nil
}

func NewBigConfig(filepath string) (*BigConfig, error) {
	file, err := os.Open(filepath)

	if err != nil {

		fmt.Println("Failed to open file , ", err)

		return nil, errors.New("failed to open file path")

	}

	filebyte, err := ioutil.ReadAll(file)

	if err != nil {
		fmt.Println("failed to read file to byte, ", err)

		return nil, errors.New("failed to read file to byte")
	}

	var BgConfig BigConfig

	err = json.Unmarshal(filebyte, &BgConfig)

	if err != nil {

		fmt.Println("Failed to unmarshal bytes , ", err)

		return nil, errors.New("failed to Unmarshal bytes")
	}

	fmt.Println("bigconfig", &BgConfig)
	return &BgConfig, nil

}
