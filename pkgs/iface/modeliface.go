package iface

import db "github.com/qobbysam/dealbase/pkgs/db"

//import "github.com/qobbysam/dealbase.git/pkgs/db"

type ModelIface interface {
	DoMigrations(dbin *db.DB) error
}
