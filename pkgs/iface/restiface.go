package iface

import "github.com/go-chi/chi"

type RestServiceInterface interface {
	RegisterRestService() (chi.Router, string)
	//StartRestService()
}
