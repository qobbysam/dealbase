package iface

type EventServiceInterface interface {
	RegisterEventService()
	StartEventService()
}
