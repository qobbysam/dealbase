package appbrain

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/go-chi/httplog"
	"github.com/qobbysam/dealbase/pkgs/config"
	db "github.com/qobbysam/dealbase/pkgs/db"
	"github.com/qobbysam/dealbase/pkgs/hub"
	"github.com/qobbysam/dealbase/pkgs/logging"
	"github.com/qobbysam/dealbase/pkgs/micro/models"
	"github.com/qobbysam/dealbase/pkgs/restserver"
	"github.com/qobbysam/dealbase/pkgs/staticserver"
	"github.com/qobbysam/dealbase/pkgs/websocketserver"
)

type AppBrain struct {
	DBconn          *db.DB
	RestServer      *restserver.RestServer
	WebSocketServer *websocketserver.WebSocketServer
	StaticServer    *staticserver.StaticServer
	//RestServiceHandler *restservicehandler.RestServiceHandler
	//EventStreamHandler *eventstreamhandler.EventStreamHandler
	Logger    *logging.BigLogger
	bootlevel int
	Config    *config.BigConfig

	SharedCtx   context.Context
	Router      chi.Router
	StopChan    chan struct{}
	NewChanPool chan struct{}
	Address     string

	WSConnector  *hub.WSConnector
	AppConnector *hub.AppConnector
}

func (ap *AppBrain) Init() error {

	ap.Logger.WriteInfo("Starting Init")

	err := ap.Logger.Init()
	if err != nil {
		fmt.Println("")
		return err
	}

	ap.bootlevel++

	err = ap.DBconn.Init()

	if err != nil {
		fmt.Println("")

		return err
	}

	ap.bootlevel++

	err = ap.RestServer.Init()

	if err != nil {
		fmt.Println("")
		return err
	}
	fmt.Println("RestServerInit called")
	ap.bootlevel++

	err = ap.WebSocketServer.Init()

	if err != nil {
		fmt.Println("")
		return err
	}

	err = ap.DBconn.Writer.CreateDefaultGroups()
	if err != nil {
		fmt.Println("msg from create", err)

	}
	ap.bootlevel++

	return nil

}
func (ab *AppBrain) BuildRoutes() chi.Router {

	router := chi.NewRouter()

	//router := chi.NewRouter()

	//Group1 := router.Group(func(r chi.Router) {

	//r.Mount("/api")
	//build static routes
	router.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		//AllowedOrigins:   []string{"https://*", "http://*"},
		AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))
	logger := httplog.NewLogger("RestServices", httplog.Options{
		JSON: true,
	})

	router.Use(httplog.RequestLogger(logger))
	out_router := ab.StaticServer.BuildRoutes(router)

	//build ws routes

	ws_in_route := ab.WebSocketServer.BuildRoutes(out_router)
	//build restroutes

	rest_routes := ab.RestServer.BuildRoutes(ws_in_route)

	// router.Mount("/", ab.StaticServer.Router)

	// router.Mount("/ws", ab.WebSocketServer.Router)

	// senior := ab.RestServer.Routes(router)

	// router.Mount("/api", ab.RestServer.Router)
	//	})

	return rest_routes
}

// func (ab *AppBrain) RegisterMigrations() error {

// 	// restserver_migrations := ab.RestServer.BuildMigrations()

// 	// err := ab.registermigrations(restserver_migrations)

// 	// if err != nil {

// 	// }

// 	// socketserver_migrations := ab.WebSocketServer.BuildMigrations()

// 	// err = ab.registerMigrations(socketserver_migrations)
// 	// if err != nil {

// 	// }
// }

func (ap *AppBrain) StartApp(errorchan chan error, wg sync.WaitGroup) {

	local_done := make(chan struct{}, 10)
	wg.Add(1)
	go func() {
		log.Println("calling hub run")
		ap.WebSocketServer.Run(ap.AppConnector, ap.WSConnector, local_done)
	}()
	ap.WebSocketServer.WSWG.Wait()
	ap.Logger.WriteInfo("Bigserver started on port , " + ap.Address)
	ap.Router = ap.BuildRoutes()
	//ap.RestServer.RegisterRoutes()

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		route = strings.Replace(route, "/*/", "/", -1)
		fmt.Printf("%s %s\n", method, route)
		return nil
	}

	if err := chi.Walk(ap.Router, walkFunc); err != nil {
		fmt.Printf("Logging err: %s\n", err.Error())
	}
	err := http.ListenAndServe(ap.Address, ap.Router)

	if err != nil {
		msg := fmt.Sprint("failed to start Rest server , ", err)
		ap.Logger.WriteError(msg)
		errorchan <- err
		wg.Done()

	}

	go func() {
		for msg := range local_done {
			//fmt.Println(msg)
			ap.NewChanPool <- msg
		}
	}()
	ap.Logger.WriteInfo("BootUp Done")

	wg.Wait()

}

func (ap *AppBrain) CreateDefaults() error {

	// moderatorm := auth.UserGroup{
	// 	ID:    "iPwbPdD4KEznHdppmzL2TV",
	// 	Name:  "MODERATORM",
	// 	Roles: "moderatorm",
	// }

	// moderatorw := auth.UserGroup{
	// 	ID:    "PX4EeEndXPUiKqXQ59n4Lm",
	// 	Name:  "MODERATORW",
	// 	Roles: "moderatorw",
	// }

	// appsu := auth.UserGroup{
	// 	ID:    "TnCviHL72CMb6jktuHeUqd",
	// 	Name:  "APPSU",
	// 	Roles: "appsu",
	// }

	// seller := auth.UserGroup{
	// 	ID:    "jH54rbKmeuuoMfLZWb79KK",
	// 	Name:  "DEALSELLER",
	// 	Roles: "dealseller",
	// }

	// buyer := auth.UserGroup{
	// 	ID:    "dEqLm3pKhyKoznyuzVPcgE",
	// 	Name:  "DEALBUYER",
	// 	Roles: "dealbuyer",
	// }

	// grouplist_auth := []*auth.UserGroup{&moderatorm, &moderatorw, &appsu, &seller, &buyer}

	//err := d.GormDB.Create(grouplist_auth)

	showingroom := []models.ShowRoom{
		{ID: "ROOM1", Name: "ROOM1"},
		{ID: "ROOM2", Name: "ROOM2"},
		{ID: "ROOM3", Name: "ROOM3"},
	}

	err := ap.DBconn.Writer.GormDB.Create(&showingroom)

	return err.Error

}

func (ap *AppBrain) BootUp() {

	//pool := 1

	ap.Logger.WriteDebug("Bootup Starting")

	var wg sync.WaitGroup

	err := ap.Init()

	if err != nil {
		msg := fmt.Sprint("failed to start Rest server , ", err)
		ap.Logger.WriteError(msg)

		panic("failed to init")
	}

	err = ap.CreateDefaults()

	if err != nil {
		msg := fmt.Sprint("failed Creade defaults , ", err)
		ap.Logger.WriteError(msg)
	}
	msg := fmt.Sprint("Run Defauls Creade defaults , ", err)

	ap.Logger.WriteInfo(msg)

	//err := ap.RegisterMigrations()

	//	stopchan := make(chan struct{}, 4)

	errorchan := make(chan error, 4)

	//var wg sync.WaitGroup
	wg.Add(1)
	go ap.StartApp(errorchan, wg)
	// go func() {
	// 	ap.Logger.StartApp(stopchan, errorchan)
	// }()

	// go func() {
	// 	ap.DBconn.StartApp(stopchan, errorchan)
	// }()

	// go func() {
	// 	ap.RestServer.StartApp(stopchan, errorchan)
	// }()

	// go func() {
	// 	ap.WebSocketServer.StartApp(stopchan, errorchan)
	// }()

	//select {}

	wg.Add(1)
	go func() {
		for {
			select {
			case <-ap.StopChan:
				wg.Done()
				wg.Done()
				wg.Done()
				wg.Done()

			case err := <-errorchan:
				fmt.Println("Error received on Error chan ,  ", err)
				ap.StopChan <- struct{}{}
			case <-ap.SharedCtx.Done():
				errorchan <- errors.New("ctx done was called")
			case <-ap.NewChanPool:
				ap.AppConnector.Inmsg = make(chan hub.MSGTYPE, 100000)
				ap.WSConnector.Inmsg = make(chan hub.WSMSG, 1000000)
				ap.NewChanPool = make(chan struct{}, 10)
			}
		}
	}()

	wg.Wait()
}

func (ab *AppBrain) Routes() chi.Router {
	router := chi.NewRouter()

	// router.Mount("/", ab.StaticServer.Router)

	// router.Mount("/api", ab.RestServer.Routes())

	// router.Mount("/ws", ab.WebSocketServer.Router)

	return router
}
func NewAppBrain(cfg *config.BigConfig, loggerin *logging.BigLogger) *AppBrain {

	outbrain := AppBrain{}

	logger := loggerin

	bigctx := context.Background()

	db := db.NewDB(cfg, logger, bigctx)

	wsconn := hub.WSConnector{
		Inmsg:  make(chan hub.WSMSG, 100000),
		OutMsg: make(chan hub.MSGTYPE, 100000),
	}

	appconn := hub.AppConnector{
		Inmsg: make(chan hub.MSGTYPE, 100000),
	}
	//newchan := make(chan struct{})

	donechan := make(chan struct{}, 10)

	socketserver := websocketserver.NewSocketServer(cfg, logger, &appconn, &wsconn, donechan, bigctx)
	restserver := restserver.NewRestServer(cfg, logger, db, &appconn, bigctx)
	staticserver := staticserver.NewStaticServer(cfg)
	//	resthandler := restservicehandler.NewRestServiceHandler(cfg, logger, bigctx)

	//	eventhandler := eventstreamhandler.NewEventStreamHandler(cfg, logger, bigctx)

	//	outbrain.RestServiceHandler = resthandler

	//	outbrain.EventStreamHandler = eventhandler
	outbrain.DBconn = db
	outbrain.RestServer = restserver
	outbrain.WebSocketServer = socketserver
	outbrain.bootlevel = 0
	outbrain.Logger = logger
	outbrain.SharedCtx = bigctx
	//outbrain.Router = router
	outbrain.Address = cfg.AppBrainConfig.Address
	outbrain.StaticServer = staticserver
	outbrain.WSConnector = &wsconn
	outbrain.AppConnector = &appconn

	outbrain.NewChanPool = donechan

	outbrain.Logger.WriteInfo("App brain created successfully")

	//outbrain.BuildMigrations()

	return &outbrain
}
