package internal

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"github.com/qobbysam/dealbase/pkgs/appbrain"
	"github.com/qobbysam/dealbase/pkgs/config"
	"github.com/qobbysam/dealbase/pkgs/logging"
	"github.com/qobbysam/dealbase/pkgs/subrain"
)

const (
	SUBRAIN  string = "subrain"
	APPBRAIN string = "appbrain"
)

func StartApp(braintype string) {
	logger := logging.NewBigLogger()
	fmt.Println("starting this application", braintype)
	var wg sync.WaitGroup

	//logger := loger
	//logger := logging.NewBigLogger()

	exdir, _ := os.Executable()

	basedir := filepath.Dir(exdir)

	appconfigfile := filepath.Join(basedir, "data", "config", "appconfig.json")

	suconfigfile := filepath.Join(basedir, "data", "config", "suconfig.json")

	switch braintype {
	case SUBRAIN:

		cfg, err := config.NewBigConfig(suconfigfile)

		if err != nil {
			fmt.Println("could not load config , ", err)
			panic("config failed")
		}

		subrain := subrain.NewSuBrain(cfg, logger)

		subrain.Init()
		subrain.BootUp()

	case APPBRAIN:
		cfg, err := config.NewBigConfig(appconfigfile)
		DBPathReader := filepath.Join(basedir, "DB", cfg.DBReaderConfig.SqliteConfig.DBName)
		DBPathWriter := filepath.Join(basedir, "DB", cfg.DBWriterConfig.SqliteConfig.DBName)
		cfg.DBReaderConfig.SqliteConfig.DBName = DBPathReader
		cfg.DBWriterConfig.SqliteConfig.DBName = DBPathWriter
		fmt.Println("Server Adress", cfg.RestServerConfig.Address)
		if err != nil {
			fmt.Println("could not load config , ", err)
			panic("config failed")
		}
		cfg.StaticServerConfig.BaseDir = basedir
		appbrain := appbrain.NewAppBrain(cfg, logger)

		// err = appbrain.Init()
		// if err != nil {
		// 	fmt.Println("could not Init App , ", err)
		// 	panic("config failed")
		// }
		wg.Add(1)
		go func() {
			appbrain.BootUp()
		}()

	default:
		fmt.Println("Unknown method run")
		os.Exit(1)

	}

	wg.Wait()
}
