package main

import (
	"flag"
	"fmt"

	"github.com/qobbysam/dealbase/internal"
)

func main() {

	start_string := flag.String("st", "app", "start brain type")
	flag.Parse()
	fmt.Println("starting this ,  ", *start_string)

	internal.StartApp(*start_string)

}
